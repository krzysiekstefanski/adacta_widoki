import {
    showSettings,
    getInputWidth,
    tabsChange,
    toggleSidebarMenu,
    changeSettingsPanel,
    backToSettingsMenu,
    changeDatabasePanel
} from "../models/Settings";

import {activateCalendar, heghtsAdjustment} from "../models/Models";
import {addDataSet} from "../models/Files.js";

export const elements = {
    settingsInMenu: $(".navbar-nav__item__link--settings"),
    userAvatarHTML: $(".avatar__block__image-container--dashboard"),
    sidebarIconHTML: $(".sidebar-icon"),
    sidebarIconWidth: $(".sidebar-icon").width(),
    checkmarkHTML: $(".checkmark"),
    taskHTML: $("#nearest-dates .panel__task"),
    sidebarMenuItemHTML: $(".dashboard__block--regular"),
    accountOptionsTabsHTML: $(".nav-link active[role='tab']"),
    moduleHTMLWidth: $(".module").width(),
    avatarContainer: $(".avatar__block__image-container"),
};

let nm = 0;

export function clearModule() {
    console.log("init func: clearModule");
    $(".module").children().empty();
}

export function loadModule(name, functionName) {
    console.log("init func: loadModule");
    $(".module").children().load(name + ".php", function() {
        functionName();
    });
}

export function clearAndAddStylesheets (name) {
    console.log("init func: clearAndAddStylesheets");
    $(".style-module").remove();
    $("head").append('<link class="style-module" rel="stylesheet" href="css/' + name + '.css">');
}

export function removeActiveClass(name) {
    console.log("init func: removeActiveClass");
    name.each(function() {
        $(this).removeClass("active")
    })
}

export function changeHeight (element, elementVar) {
    console.log("init func: changeHeight");
    console.log(element);
    console.log(" ");
    elementVar = element.width();
    element.css("height", elementVar + "px");
}

export function inheritWidthFromHeight (element) {
    console.log("init func: inheritWidthFromHeight");
    console.log(element);
    console.log(" ");
    let k = element.height();
    element.width(k);
}

export function swipeBetweenTwoPanels() {
    console.log("init func: swipeBetweenTwoPanels");
    console.log(" ");
    let width;
    $(".swiper").click(function() {
      width = $(".nearest-dates-container").width() - 15;
      $(".swiper").parent().removeClass("active");
      $(this).parent().addClass("active");
      if ($(".nearest-dates-container").is(".active")) {
        $(".nearest-dates-container").animate({ marginLeft: '0'}, 1000);
      } else {
        $(".nearest-dates-container").animate({ marginLeft: '-' + width}, 1000);
      }
    });
    $(".nearest-dates-container").on("swipeleft", function() {
        $(".recently-added-documents-container").addClass("active");
        $(".nearest-dates-container").removeClass("active");
        width = $(".nearest-dates-container").width() +30;
        $(".nearest-dates-container").animate({ marginLeft: '-' + width}, 1000);
    });
    $(".recently-added-documents-container").on("swiperight", function() {
        $(".nearest-dates-container").addClass("active");
        $(".recently-added-documents-container").removeClass("active");
        $(".nearest-dates-container").animate({ marginLeft: '0'}, 1000);
    });
}

export function resetPanelsPosition() {
  $(".swiper").parent().removeClass("active");
  $(".nearest-dates-container").addClass("active").removeAttr('style');;
}

function sortingPanel (element, elementsList, data) {
    console.log("init func: sortingPanel");
    let list = elementsList;
    let listItem = element;

    listItem.detach().sort(function(a, b) {
        let astts = $(a).data(data);
        let bstts = $(b).data(data);
        return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
    });

    list.append(listItem);
}

function clickPanel (elementHTML) {
    console.log("init func: clickPanel");
    let el = elementHTML;

    el.click(function () {
        let closestID = $(this).closest("div[id]");
        console.log(nm);
        console.log($(window).width());
        if ($(window).width() > 768) {
            closestID.next().addClass("active").animate({ marginLeft: '0px', opacity: 1 }, 1000);
            if (nm == 3) {
                closestID.next().next().addClass("active").animate({ marginLeft: '0px', opacity: 1 }, 1000);
            }
            if (closestID.next().next().is("#files-tiles")) {
                closestID.next().next().removeAttr( 'style' );
            }
            if (closestID.prev().is("[id]")) {
                closestID.prev().animate({ marginLeft: "-"+(closestID.prev().width()+30)+"px", opacity: 0 }, 1000, function() {
                    $(this).removeClass("active");
                });
            }
        } else {
            console.log("window < 768px");
            closestID.next().addClass("active").animate({ marginLeft: '0px', opacity: 1 }, 1000);
            closestID.animate({ marginLeft: "-"+(closestID.width()+30)+"px", opacity: 0 }, 1000, function() {
                $(this).removeClass("active");
            });
        }

    });
}

function clickBackPanel() {
    console.log("init func: clickBackPanel");
    $(".panel__title__icon").click(function() {
        if ($(window).width() > 768) {
            $(this).closest("div[id]").animate({marginLeft: '1000px', opacity: 0}, 1000, function () {
                $(this).closest("div[id]").removeClass("active")
            });
            $(this).closest("div[id]").nextUntil("#files-search").animate({
                marginLeft: '1000px',
                opacity: 0
            }, 1000, function () {
                $(this).closest("div[id]").removeClass("active")
            });
            if ($(this).closest("div[id]").prev().is("[id]")) {
                if ($(this).closest("div[id]").prev().hasClass("active")) {
                    $(this).closest("div[id]").prev().prev().addClass("active").animate({
                        marginLeft: '0px',
                        opacity: 1
                    }, 1000);
                } else {
                    $(this).closest("div[id]").prev().addClass("active").animate({marginLeft: '0px', opacity: 1}, 1000);
                    $(this).closest("div[id]").prev().prev().addClass("active").animate({
                        marginLeft: '0px',
                        opacity: 1
                    }, 1000);
                }
            }
        } else {
            console.log("window < 768px");
            $(this).closest("div[id]").prev().addClass("active").animate({ marginLeft: '0px', opacity: 1 }, 1000);
            $(this).closest("div[id]").animate({ marginLeft: '1000px', opacity: 0 }, 1000, function() {
                $(this).removeClass("active");
            });
        }
    });
}

function numberOfPanelsControl () {
    console.log("init func: numberOfPanelsControl");
    let clientsList = $("#files-clients").outerWidth();
    let cases = $("#files-cases").outerWidth();
    let documents = $("#files-documents").outerWidth();
    let module = $(".module").width();

    console.log(clientsList);
    console.log(cases);
    console.log(documents);
    console.log(clientsList + cases + documents);
    console.log(module);

    if (clientsList + cases + documents < module) {
        nm = 3;
    } else {
        nm = 2;
    }
    return nm;
}

function clickChangePanel (elementHTML, panelToOpen) {
    console.log("init func: clickChangePanel");
    elementHTML.click(function(){
        $(".panels-block > div:not(:last)").removeClass("active");
        $(".panels-block > div:not(:last)").removeAttr( 'style' );
        panelToOpen.addClass("active");
        if (!panelToOpen.length) {
            removeActiveClass(elements.sidebarMenuItemHTML);
            elements.sidebarMenuItemHTML.parent().find("#sidebar-3").addClass("active");
            clearModule();
            clearAndAddStylesheets("clients-database");
            loadModule("clients-database", ifDatabaseSuccess);
        }
    })
}

function showTiles (panelID) {
    console.log("init func: showTiles");
    panelID.click(function () {
        $(this).closest("div[id]").next().next().addClass("active");
    });
}

function clickDropdownArrow() {
    $(".panel__title__arrow-down").click(function() {
        $(this).toggleClass("active");
        $(".panel__title__dropdown-panel").toggleClass("active");
    });
}

function getSliderRangeContent (id) {
    console.log("init func: getSliderRangeContent");
    id.find(".ui-slider-handle").attr('data-content-1', id.slider( "values", 0 ));
    id.find(".ui-slider-handle").attr('data-content-2', id.slider( "values", 1 ));
    id.attr('data-content-3', '');
}

export function ifFilesSuccess() {
    console.log("init func: ifFilesSuccess");

    numberOfPanelsControl();
    addDataSet($(".client-name"), $(".panel__client"));
    addDataSet($(".case-name"), $(".panel__case"));
    addDataSet($(".panel__task__text-block .address-text"), $(".panel__task"));

    let sortByNumHTML = $(".panel__title__sort__ico--lttr");
    sortByNumHTML.click(function() {
        if ($(this)[0] == sortByNumHTML[0]) {
            sortingPanel($(".panel__case"), $(".panel__case").parent(), 'sort');
        } else {
            sortingPanel($(".panel__task"), $(".panel__task").parent(), 'sort');
        }
    });
    toggleSidebarMenu($("#files-search"));
    sortingPanel($(".panel__client"), $(".panel__client").parent(), 'sort');
    clickPanel($(".panel__client"));
    clickPanel($(".panel__case"));
    clickPanel($(".panel__task"));
    showTiles($(".panel__task"));
    clickChangePanel($(".add-new-case-btn"), $("#files-add-new-case"));
    clickChangePanel($(".add-new-document-btn"), $("#files-add-new-document"));
    clickChangePanel($(".add-new-client-btn"), $("#clients-database"));
    $(".go-back-btn").click(function() {
        removeActiveClass(elements.sidebarMenuItemHTML);
        clearModule();
        clearAndAddStylesheets("virtual-files");
        loadModule("main", ifMainSuccess);
    });
    clickBackPanel();
    clickDropdownArrow();
}

function ifMainSuccess() {
    console.log("init func: ifMainSuccess");
    location.reload();
}

export function ifCalendarSuccess() {
    console.log("init func: ifCalendarSuccess");
    activateCalendar();
    heghtsAdjustment();
}

export function ifDatabaseSuccess() {
    console.log("init func: ifDatabaseSuccess");
    toggleSidebarMenu($('#clients-database-sidebar'));
    changeDatabasePanel($("#clients-database"));

    $( "#slider-range-1" ).slider({
        range: true,
        min: 0,
        max: 25,
        values: [ 0, 25 ],
        slide: function( event, ui ) {
            $("#slider-range-1 .ui-slider-handle").attr('data-content-1', ui.values[ 0 ]);
            $("#slider-range-1 .ui-slider-handle").attr('data-content-2', ui.values[ 1 ]);

            if ($("#slider-range-1 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-1 .ui-slider-handle").attr('data-content-2') == 25) {
                $("#slider-range-1 .ui-slider-handle").attr('data-content-1', '');
                $("#slider-range-1 .ui-slider-handle").attr('data-content-2', '');
                $("#slider-range-1").attr('data-content-3', 'dowolny');
            } else {
                $("#slider-range-1").attr('data-content-3', '');
            }
        }
    });

    $( "#slider-range-2" ).slider({
        range: true,
        min: 0,
        max: 40,
        values: [ 0, 40 ],
        slide: function( event, ui ) {
            $("#slider-range-2 .ui-slider-handle").attr('data-content-1', ui.values[ 0 ]);
            $("#slider-range-2 .ui-slider-handle").attr('data-content-2', ui.values[ 1 ]);

            if ($("#slider-range-2 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-2 .ui-slider-handle").attr('data-content-2') == 40) {
                $("#slider-range-2 .ui-slider-handle").attr('data-content-1', '');
                $("#slider-range-2 .ui-slider-handle").attr('data-content-2', '');
                $("#slider-range-2").attr('data-content-3', 'dowolny');
            } else {
                $("#slider-range-2").attr('data-content-3', '');
            }
        }
    });
    $( "#slider-range-3" ).slider({
        range: true,
        min: 0,
        max: 25,
        values: [ 0, 25 ],
        slide: function( event, ui ) {
            $("#slider-range-3 .ui-slider-handle").attr('data-content-1', ui.values[ 0 ]);
            $("#slider-range-3 .ui-slider-handle").attr('data-content-2', ui.values[ 1 ]);

            if ($("#slider-range-3 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-3 .ui-slider-handle").attr('data-content-2') == 25) {
                $("#slider-range-3 .ui-slider-handle").attr('data-content-1', '');
                $("#slider-range-3 .ui-slider-handle").attr('data-content-2', '');
                $("#slider-range-3").attr('data-content-3', 'dowolny');
            } else {
                $("#slider-range-3").attr('data-content-3', '');
            }
        }
    });
    $( "#slider-range-4" ).slider({
        range: true,
        min: 0,
        max: 25,
        values: [ 0, 25 ],
        slide: function( event, ui ) {
            $("#slider-range-4 .ui-slider-handle").attr('data-content-1', ui.values[ 0 ]);
            $("#slider-range-4 .ui-slider-handle").attr('data-content-2', ui.values[ 1 ]);

            if ($("#slider-range-4 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-4 .ui-slider-handle").attr('data-content-2') == 25) {
                $("#slider-range-4 .ui-slider-handle").attr('data-content-1', '');
                $("#slider-range-4 .ui-slider-handle").attr('data-content-2', '');
                $("#slider-range-4").attr('data-content-3', 'dowolny');
            } else {
                $("#slider-range-4").attr('data-content-3', '');
            }
        }
    });

    getSliderRangeContent($("#slider-range-1"));
    getSliderRangeContent($("#slider-range-2"));
    getSliderRangeContent($("#slider-range-3"));
    getSliderRangeContent($("#slider-range-4"));

    if ($("#slider-range-1 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-1 .ui-slider-handle").attr('data-content-2') == 25) {
        $("#slider-range-1 .ui-slider-handle").attr('data-content-1', '');
        $("#slider-range-1 .ui-slider-handle").attr('data-content-2', '');
        $("#slider-range-1").attr('data-content-3', 'dowolny');
    }

    if ($("#slider-range-2 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-2 .ui-slider-handle").attr('data-content-2') == 40) {
        $("#slider-range-2 .ui-slider-handle").attr('data-content-1', '');
        $("#slider-range-2 .ui-slider-handle").attr('data-content-2', '');
        $("#slider-range-2").attr('data-content-3', 'dowolny');
    }

    if ($("#slider-range-3 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-3 .ui-slider-handle").attr('data-content-2') == 25) {
        $("#slider-range-3 .ui-slider-handle").attr('data-content-1', '');
        $("#slider-range-3 .ui-slider-handle").attr('data-content-2', '');
        $("#slider-range-3").attr('data-content-3', 'dowolny');
    }

    if ($("#slider-range-4 .ui-slider-handle").attr('data-content-1') == 0 && $("#slider-range-4 .ui-slider-handle").attr('data-content-2') == 25) {
        $("#slider-range-4 .ui-slider-handle").attr('data-content-1', '');
        $("#slider-range-4 .ui-slider-handle").attr('data-content-2', '');
        $("#slider-range-4").attr('data-content-3', 'dowolny');
    }

    $(".btn-red").each(function() {
        $(this).click(function() {
            $(this).parent().parent().parent().parent().remove();
        })
    });

    $(".btn-primary").each(function() {
        $(this).click(function() {
            $("#clients-list").removeClass("active");
            $("#clients-settings").addClass("active");
        })
    });

    $(".btn-violet").each(function() {
        $(this).click(function() {
            $("#clients-list").removeClass("active");
            $("#clients-settings").addClass("active");
        })
    });

    $(".go-back-btn").click(function() {
        removeActiveClass(elements.sidebarMenuItemHTML);
        clearModule();
        clearAndAddStylesheets("virtual-files");
        loadModule("main", ifMainSuccess);
    });

}

export function ifSettingsSuccess() {
    console.log("init func: ifSettingsSuccess");
    showSettings(0);
    tabsChange();
    changeSettingsPanel($("#settings"));
    toggleSidebarMenu($('#search'));
}
