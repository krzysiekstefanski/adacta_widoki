import {elements, clearModule, clearAndAddStylesheets, loadModule, removeActiveClass, changeHeight, ifFilesSuccess, ifCalendarSuccess, ifDatabaseSuccess, ifSettingsSuccess, inheritWidthFromHeight, swipeBetweenTwoPanels, logoMarginAuto, resetPanelsPosition} from "./views/base";

changeHeight(elements.sidebarIconHTML, elements.sidebarIconWidth);
inheritWidthFromHeight(elements.avatarContainer);

function draggablePanels() {
    console.log("init func: draggable");
    $( ".panel__main-panel__show-in-main-block__block-panel" ).sortable({
        connectWith: ".panel__main-panel__show-in-main-block__block-panel__order",
        handle: ".grabber",
        placeholder: "portlet-placeholder ui-corner-all"
    });
}

$( window ).resize(function() {
    changeHeight(elements.sidebarIconHTML, elements.sidebarIconWidth);
    elements.moduleHTMLWidth = $(".module").width();
    inheritWidthFromHeight(elements.avatarContainer);
    resetPanelsPosition()
});

elements.taskHTML.click(function() {
    if ($(this).find(".checkmark").hasClass("checked")) {
            $(this).find(".checkmark").removeClass("checked");
            $(this).removeClass("task-done");
    } else {
            $(this).find(".checkmark").addClass("checked");
            $(this).addClass("task-done");
    }
});

elements.sidebarMenuItemHTML.click(function() {
    if ($(this).attr('id')) {
        removeActiveClass(elements.sidebarMenuItemHTML);
        $(this).addClass("active");
        if ($(this).attr('id') == "sidebar-1") {
            clearModule();
            clearAndAddStylesheets("virtual-files");
            loadModule("virtual-files", ifFilesSuccess);
        } else if ($(this).attr('id') == "sidebar-2") {
            clearModule();
            clearAndAddStylesheets("calendar-schelude");
            loadModule("calendar-schelude", ifCalendarSuccess);
        } else if ($(this).attr('id') == "sidebar-3") {
            clearModule();
            clearAndAddStylesheets("clients-database");
            loadModule("clients-database", ifDatabaseSuccess);
        }
    }
});

elements.userAvatarHTML.click(function() {
    removeActiveClass(elements.sidebarMenuItemHTML);
    clearModule();
    clearAndAddStylesheets("settings");
    loadModule("settings", ifSettingsSuccess);
});

elements.settingsInMenu.click(function() {
    removeActiveClass(elements.sidebarMenuItemHTML);
    clearModule();
    clearAndAddStylesheets("settings");
    loadModule("settings", ifSettingsSuccess);
});

swipeBetweenTwoPanels();
