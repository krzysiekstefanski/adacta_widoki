<!-- dropdown -->
<!-- start -->

<div class="dropdown dropdown__gray dropdown--normal">
   <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"   aria-expanded="false">Wybierz klienta</button>
   <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
       <a class="dropdown-item" href="#">Action</a>
       <a class="dropdown-item" href="#">Another action</a>
       <a class="dropdown-item" href="#">Something else here</a>
   </div>
</div>

<!-- end -->

<!-- input -->
<!-- start -->

<label for="inputSearch2" class="search-label float-left">
    <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wpisz">
</label>

<!-- end -->

<!-- radio -->
<!-- start -->

<div class="radio">
    <label class="radio-label">
        <input type="radio" name="optradio" checked="">
        <div class="checkmark"></div>
        Firma
    </label>
    <label class="radio-label">
        <input type="radio" name="optradio">
        <div class="checkmark"></div>
        Indywidualny
    </label>
</div>

<!-- end -->

<!-- section name block -->
<!-- start -->

<div class="col-12 dashboard__block dashboard__block--top-small">
    <div class="float-left h-100 w-25 d-flex align-items-center">
        <div class="logo-img__block">
            <div class="logo-img__block--square">
                <img src="./img/adacta-favicon.svg" alt="" class="img-fluid logo-img">
            </div>
        </div>
        <div class="logo-text__block">
            <p>section name</p>
        </div>
    </div>
</div>

<!-- end -->

<!-- avatar block -->
<!-- start -->

<!-- with two options: dashboard, panel -->

<div class="avatar__block">
    <div class="avatar__block__image-container">
        <img src="img/default-avatar.svg" alt="Avatar Użytkownika" class="img-fluid">
    </div>
</div>

<!-- end -->

<!-- navigation block -->
<!-- start -->

<nav class="navigation float-right">
    <ul class="nav navbar-nav flex-md-row">
        <a href="#"><li><p>wiadomości</p></li></a>
        <a href="#"><li><p>opcje</p></li></a>
        <a href="#"><li><p>wyloguj</p></li></a>
    </ul>
</nav>

<!-- end -->