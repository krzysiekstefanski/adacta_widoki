<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/login-page.css">
    <link rel="shortcut icon" href="img/adacta-favicon.svg" type="image/x-icon">
    <title>ADACTA Login</title>

    <script src="js/jquery-3.3.1.min.js"></script>
</head>

<body class="login-page">
    <div class="w-100 h-100 d-flex justify-content-center align-items-center">
        <div class="login-box">
            <div class="row">
                <div class="col-12 mb-5">
                    <img src="./img/adacta-logo.svg" alt="ADACTA Logo">
                </div>
            </div>
            <div class="row">
                <div class="col-12 mb-5">
                    <label for="inputName1" class="login-label w-100">
                        <span class="login-label__icon login-label__icon--name"></span>
                        <input type="name" class="form-control login-label__name" id="inputName1" placeholder="nazwa użytkownika">
                        <span class="login-label__checkmark"></span>
                    </label>
                    <label for="inputPass1" class="login-label w-100">
                        <span class="login-label__icon login-label__icon--password"></span>
                        <input type="password" class="form-control login-label__password" id="inputPass1" placeholder="hasło">
                        <span class="login-label__checkmark"></span>
                    </label>
                    <div class="w-100 mt-2">
                        <span class="pass-recovery float-right">Nie pamiętam hasła</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 d-flex justify-content-center">
                    <button type="button" class="btn btn-outline-light btn-lg btn-login">ok</button>
                </div>
            </div>
        </div>
    </div>
    <script src="js/bundle.js"></script>
</body>

</html>