import {clearAndAddStylesheets, clearModule, elements, ifSettingsSuccess, loadModule} from "../views/base";

export function showSettings(index) {
    console.log("init func: showSettings");
    let settingsChildrenHTML = $("#settings .active").children();
    settingsChildrenHTML.removeClass("d-flex");
    settingsChildrenHTML.eq(index).addClass("d-flex");
}

export function getInputWidth (container) {
    console.log("init func: getInputWidth");
    container.each(function () {
        if ($(this).find('input').is(":not(:radio)")) {
            let inputPlaceholderHTML = $(this).find('input').attr('placeholder');
            let inputLabelHTML = $(this).find('p').text();
            let inputWidthHTML = $(this).width();
            let _$tmpSpan = $('<span/>').html(inputPlaceholderHTML).css({
                    position: 'absolute',
                    left: -9999,
                    top: -9999
                }).appendTo('body'),
                textWidth = _$tmpSpan.width();
            _$tmpSpan.text('');
            _$tmpSpan.text(inputLabelHTML);
            let labelWidth = _$tmpSpan.width();
            _$tmpSpan.remove();
            $(this).css('min-width', labelWidth + 20 + 'px');
            $(this).css('width', textWidth + 20 + 'px');
            if (labelWidth > textWidth) {
                $(this).css({
                    'width': labelWidth + 'px',
                })
            }
            if (inputWidthHTML < textWidth) {
                $(this).css({
                    'width': textWidth + 'px',
                })
            }
        }
    });
}

export function inheritWidth (from, to) {
    console.log("init func: inheritWidth");
    from.each(function () {
        let i = $(this).data("inherit");
        let fromWidth = $(this).width();
        let fromMinWidth = $(this).css("min-width").replace(/[^-\d\.]/g, '');

        if (fromWidth < fromMinWidth) {
            fromWidth = fromMinWidth
        }

        to.each(function() {
            let x = $(this).data("inherit");

            if (x === i) {
                $(this).width(fromWidth);
            }
        });
    });
}

export function tabsChange() {
    console.log("init func: tabsChange");
    console.log($(".nav-link[role='tab']"));
    console.log($(".tab-pane[role='tabpanel']"));
    $(".nav-link[role='tab']").click(function() {
        $(".nav-link[role='tab']").each(function() {
            $(this).removeClass("active");
        });
        $(".tab-pane[role='tabpanel']").each(function() {
            $(this).removeClass("active");
        });
        var id = $(this).attr("href");
        console.log(id);
        $(this).addClass("active");
        $(id).addClass("active")
    });
}

export function toggleSidebarMenu(id) {
    console.log("init func: toggleSidebarMenu");
    $(".search-panel-toggler").click(function() {
        id.toggleClass("shut");
    });
}

function emptyFunction() {
    console.log("init func: emptyFunction");
    location.reload();
}

function addNewAddressInputs(buttonId) {
    console.log("init func: addNewAddressInputs");
    console.log($("#"+buttonId));
    let arr = ['nazwa adresu','ulica i numer','kod pocztowy','miejscowość','kraj']
    console.log(arr);
    let btn;
    btn = $("#"+buttonId).find(".add-new-address.active");
    console.log(btn);
    btn.click(function() {
        $(this).parent().append('<div class="panel__clients-databse__owner-data add-new-address d-flex flex-column justify-content-center"><div class="d-flex align-items-center"><p>dodaj kolejny</p></div></div>');
        let container = $(this).parent().parent();
        container.append('<div class="col-12 d-flex flex-column flex-md-row justify-content-start">');
        for (let i = 0; i <= 4; i++) {
            container.children().eq(-1).append('<div class="panel__clients-databse__owner-data d-flex flex-column"><p class="d-block d-md-none">' + arr[i] + '</p><label for="inputSearch2" class="search-label float-left"><input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="" readonly="">')
        }
        $(this).detach().appendTo(container.children().eq(-1))
    });
}

export function showSidebarMenu() {
    console.log("init func: showSidebarMenu");
    if ($("#search").hasClass("d-none")) {
        $("#search").removeClass("d-none");
    }
}

export function changeSettingsPanel(container) {
    console.log("init func: changeSettingsPanel");
    if ($(".panel").attr("id") == "settings-menu") {
        $("#search").addClass("d-none");
        $("#search").addClass("shut");
    }
    container.find(".btn-square").click(function() {
        $("#search").removeClass("d-none");
        if ($(this).data("name") == "menu") {
            clearModule();
            clearAndAddStylesheets("virtual-files");
            loadModule("main", emptyFunction);
        } else {
            let name = $(this).data("name");
            $("#settings .panel-container").empty();
            $("#settings .panel-container").load("partials/settings/"+ name + ".php", function() {
                tabsChange();
            });
        }
    });
}

export function changeDatabasePanel(container) {
    console.log("init func: changeDatabasePanel");
    container.find(".btn-primary").click(function() {
        $("#clients-database .panel-container").empty();
        $("#clients-database .panel-container").load("partials/clients-database/clients-settings.php", function() {
            tabsChange();
            addNewAddressInputs("addresses");
            addNewAddressInputs("contact-persons");
            console.log($(".footer-block").find(".panel--buton").eq(-1).find(".btn").eq(-1));
            $(".footer-block").find(".panel--buton").eq(-1).find(".btn").eq(-1).click(function() {
                emptyFunction();
            })
        });
    });
}
