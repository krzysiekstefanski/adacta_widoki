export function activateCalendar() {
    $('#calendar').fullCalendar({
        defaultView: 'agendaWeek',

        selectable: true,

        locale: "pl",

        height: "auto",

        contentHeight: "auto",

        minTime: "07:00:00",

        maxTime: "20:00:00",

        allDayText: 'Terminy',

        header: { center: 'month,agendaWeek' }, // buttons for switching between views

        views: {
            month: { // name of view
                titleFormat: 'YYYY, MM, DD'
                // other view-specific options here
            }
        },

        dayClick: function(date) {
            alert('clicked ' + date.format());
        },

        // select: function () {
        //     alert('selected ' + startDate.format() + ' to ' + endDate.format());
        // }
    });
}

export function heghtsAdjustment() {
    let calendarHeight = $("#calendar").height();
    let dayGridHeight = $(".fc-day-grid").height();
    let headHeight = $(".fc-head").height();
    let headerToolbarHeight = $(".fc-header-toolbar").height();
    let cells = (calendarHeight - dayGridHeight - headHeight - headerToolbarHeight - 20)/26;

    $('.fc-slats tr').css("height", cells + "px");

    $( window ).resize(function() {
        calendarHeight = $("#calendar").height();
        dayGridHeight = $(".fc-day-grid").height();
        headHeight = $(".fc-head").height();
        headerToolbarHeight = $(".fc-header-toolbar").height();

        cells = (calendarHeight - dayGridHeight - headHeight - headerToolbarHeight - 20)/26;
        $('.fc-slats tr').css("height", cells + "px");
    });
}

