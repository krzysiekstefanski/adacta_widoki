export function sortingPanel (element, elementsList, data) {
    let list = elementsList;
    let listItem = element;

    listItem.detach().sort(function(a, b) {
        let astts = $(a).data(data);
        let bstts = $(b).data(data);
        return (astts > bstts) ? (astts > bstts) ? 1 : 0 : -1;
    });

    list.append(listItem);
}