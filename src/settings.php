<div class="row">
    <div class="col-12 section-name__block">
        <div class="float-left h-100 w-100 d-flex align-items-center">
            <div class="logo-img__block">
                <div class="logo-img__block--square">
                    <img src="./img/settings-gradient-ico.svg" alt="Wirtualne Akta" class="img-fluid logo-img">
                </div>
            </div>
            <div class="logo-text__block d-none d-md-block">
                <p>Opcje</p>
            </div>
            <div class="logo-nav__block d-block d-md-none float-right h-100 d-flex flex-grow-1 align-items-center">
                <nav class="navigation float-right">
                    <ul class="nav navbar-nav flex-row justify-content-end">
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--search"></div>
                            </a>
                        </li>
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--messages"></div>
                            </a>
                        </li>
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--settings"></div>
                            </a>
                        </li>
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--logout"></div>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="row flex-nowrap iphone__module-bd-hgt--x" id="settings">
    <div class="col-12 panel-container active">
        <?php require "partials/settings/menu.php"; ?>
<!--        --><?php //require "partials/settings/account.php"; ?>
<!--        --><?php //require "partials/settings/main-panel.php"; ?>
<!--        --><?php //require "partials/settings/information-portal.php"; ?>
<!--        --><?php //require "partials/settings/help.php"; ?>
<!--        --><?php //require "partials/settings/team-managment.php"; ?>
<!--        --><?php //require "partials/settings/calendar.php"; ?>
<!--        --><?php //require "partials/settings/invoice.php"; ?>
<!--        --><?php //require "partials/settings/messages.php"; ?>
    </div>
    <?php require "partials/settings/sidebar.php"; ?>
</div>
