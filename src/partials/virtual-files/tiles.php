<div class="p-3 content-height__tiles" id="files-tiles">
    <div class="h-100 d-flex flex-column justify-content-between">
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-prosecutor-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__block">
                                <p class="panel__tile__block tile-text">Powód</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                    <div class="panel__tile">
                        <div class="panel__tile__text">
                            <ul>
                                <li>Maria Figat</li>
                                <li>ul. Tumska 12/2, 71-009 Dobra</li>
                            </ul>
                            <p class="mt-2"><span>Pełnomocnik</span></p>
                            <ul>
                                <li>Kancelaria Adwokacka Anna Kowalska,</li>
                                <li>ul. Ks. Bogusława X 12/3</li>
                                <li>71-809 Szczecin</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-accused-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__block">
                                <p class="panel__tile__block tile-text red">Pozwany</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                    <div class="panel__tile">
                        <div class="panel__tile__text">
                            <ul>
                                <li>Jan Kowalski</li>
                                <li>ul. Chojecka 9, 71-009 Dobra</li>
                            </ul>
                            <p class="mt-2"><span class="red">Pełnomocnik</span></p>
                            <ul>
                                <li>Kancelaria Adwokacka Jan Nadolny,</li>
                                <li>ul. Juranda 12/3</li>
                                <li>71-856 Szczecin</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-subcject-matter-value-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__text">
                                <p><span>Wartość przedmiotu sprawy:</span> 375,00 zł</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-referent-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__text">
                                <p class="panel__tile__block tile-text"><span>Referent:</span> SSR Szymon Konieczny</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-court-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__text">
                                <p><span>Sąd:</span></p>
                                <p>Sąd Rejonowy Szczecin-Prawobrzeże i Zachód w Szczecinie</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-deparment-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__text">
                                <p class="panel__tile__block tile-text"><span>Wydział:</span> III Wydział Cywilny</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-subcject-matter-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__text">
                                <p><span>Przedmiot sprawy:</span></p>
                                <p>Inne bez symbolu i o symbolu wyżej wymienionym</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-briefcase-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__text">
                                <p><span>Tytuł teczki:</span></p>
                                <p>Maria Figat , Jan Kowalski, Inne bez symbolu i o symbolu wyżej wymienionym</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel py-1 px-2" style="background-color: #fff;">
                    <div class="panel__tile d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__tile__icon">
                                <img src="./img/tile-history-ico.svg" alt="" height="14">
                            </div>
                            <div class="panel__tile__block">
                                <p class="panel__tile__block tile-text">Historia zmian:</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                    <div class="row">
                        <div class="col-11 panel__tile content-height__history content-scroll">
                            <div class="panel__tile__history">
                                <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                <div class="panel--line-grey"></div>
                            </div>
                            <div class="panel__tile__history">
                                <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                <div class="panel--line-grey"></div>
                            </div>
                            <div class="panel__tile__history">
                                <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                <div class="panel--line-grey"></div>
                            </div>
                            <div class="panel__tile__history">
                                <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                <div class="panel--line-grey"></div>
                            </div>
                            <div class="panel__tile__history">
                                <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                <div class="panel--line-grey"></div>
                            </div>
                            <div class="panel__tile__history">
                                <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                <div class="panel--line-grey"></div>
                            </div>
                            <div class="panel__tile__history">
                                <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                <div class="panel--line-grey"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
