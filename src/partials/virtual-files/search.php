<div class="sidebar shut" id="files-search">
    <div class="search-panel-toggler d-flex flex-column justify-content-center align-items-center">
        <div class="circle"></div>
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="panel d-flex flex-column justify-content-between h-100 px-3">
        <div class="panel__search">
            <div class="row">
                <div class="col-12 py-3">
                    <div class="row">
                        <div class="col-12">
                            <span class="search-label__icon search-label__icon--text"></span>
                            <span class="form-control search-title">Wyszukiwanie</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <p class="dropdown-text">szukaj w</p>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-1">
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control input-white" id="inputSearch2" placeholder="Wpisz szukaną frazę...">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 py-3">
                    <div class="row">
                        <div class="col-12">
                            <span class="filter-label__icon filter-label__icon--text"></span>
                            <span class="form-control search-title">Filtrowanie</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <div class="dropdown w-100">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszyscy klienci</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <div class="dropdown w-100">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszystkie sprawy</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <div class="dropdown w-100">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszystkie typy dokumentów</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pb-3">
                    <p>Zakres dat</p>
                    <div class="w-50 float-left">
                        <div class="h-100 d-flex justify-content-between align-items-center pr-1">
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control input-white" data-toggle="datepicker" id="inputSearch2" placeholder="Od">
                            </label>
                        </div>
                    </div>
                    <div class="w-50 float-left">
                        <div class="h-100 d-flex justify-content-between align-items-center pl-1">
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control input-white" data-toggle="datepicker" id="inputSearch2" placeholder="Do">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pb-3">
                    <p>Źródła dokumentów:</p>
                    <div class="radio">
                        <label class="radio-label">
                            <input type="radio" name="optradio" checked>
                            <div class="checkmark"></div>
                            Wszystkie źródła
                        </label>

                        <label class="radio-label">
                            <input type="radio" name="optradio">
                            <div class="checkmark"></div>
                            Tylko Portal Informacyjny
                        </label>

                        <label class="radio-label">
                            <input type="radio" name="optradio">
                            <div class="checkmark"></div>
                            Tylko AdActa
                        </label>

                        <label class="radio-label">
                            <input type="radio" name="optradio">
                            <div class="checkmark"></div>
                            Archiwum
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel__footer">
            <div class="row">
                <div class="col-12 d-flex justify-content-between">
                    <div class="btn btn-square btn-square--small d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="add-new-document-btn content"></div>
                        </div>
                    </div>
                    <div class="btn btn-square btn-square--small d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="add-new-case-btn content"></div>
                        </div>
                    </div>
                    <div class="btn btn-square btn-square--small d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="add-new-client-btn content"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="btn btn-square btn-square--large d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="go-back-btn content d-flex justify-content-center align-items-center">
                                <div class="go-back-block">
                                    <p>powrót</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
