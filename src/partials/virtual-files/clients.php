<div class="active px-3" id="files-clients">
    <div class="my-3 shadow" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Lista klientów</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel content-height__files content-scroll px-3">
                    <div class="panel__client d-flex justify-content-start align-items-center">
                        <div class="panel__client__avatar-block">
                            <div class="client-avatar">
                                <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                            </div>
                        </div>
                        <div class="panel__client__text-block h-100">
                            <p class="client-name">Beksiński Marek</p>
                        </div>
                    </div>
                    <div class="panel__client active d-flex justify-content-start align-items-center">
                        <div class="panel__client__avatar-block">
                            <div class="client-avatar">
                                <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                            </div>
                        </div>
                        <div class="panel__client__text-block h-100">
                            <p class="client-name">Figat Maria</p>
                        </div>
                    </div>
                    <div class="panel__client d-flex justify-content-start align-items-center">
                        <div class="panel__client__avatar-block">
                            <div class="client-avatar">
                                <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                            </div>
                        </div>
                        <div class="panel__client__text-block h-100">
                            <p class="client-name">Andryszuk Mariusz</p>
                        </div>
                    </div>
                    <div class="panel__client d-flex justify-content-start align-items-center">
                        <div class="panel__client__avatar-block">
                            <div class="client-avatar">
                                <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                            </div>
                        </div>
                        <div class="panel__client__text-block h-100">
                            <p class="client-name">Andryszuk Marianna</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block px-3">
                    <div class="panel--line-grey py-3"></div>
                    <div class="panel--button d-flex justify-content-center align-items-center">
                        <button type="button" class="btn btn-white btn-outline-light add-new-client-btn">dodaj nowego klienta</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
