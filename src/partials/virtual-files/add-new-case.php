<div class="col-12" id="files-add-new-case">
    <div class="row">
        <div class="w-100 m-3 p-3 panel content-height__files--add-new-case" style="background-color:#fff">
            <div class="row">
                <div class="col-12">
                    <div class="pt-3">
                      <div class="panel__title margin-bottom-8 d-flex justify-content-start align-items-center">

                        <div class="panel__title__block">
                            <div class="panel__title__block--case-ico"></div>
                        </div>

                        <div class="input-block--column">
                          <div class="dropdown dropdown__primary dropdown--small">
                              <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SPRAWA W TOKU (I instancja)</button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                          </div>
                        </div>

                        <div class="input-block--column">
                            <label for="inputSearch2" class="search-label float-left">
                                <input type="text" class="form-control input input--small input__primary" id="inputSearch2" placeholder="Sygnatura" readonly>
                            </label>
                        </div>

                        <div class="input-block--column">
                          <div class="dropdown dropdown__gray dropdown--small">
                              <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz klienta</button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                          </div>
                        </div>

                        <div class="input-block--column">
                          <div class="dropdown dropdown__green dropdown--small">
                              <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Powód</button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                          </div>
                        </div>

                        <div class="input-block--column">
                          <p>vs</p>
                        </div>

                        <div class="input-block--column">
                            <label for="inputSearch2" class="search-label float-left">
                                <input type="text" class="form-control input input--small input__gray" id="inputSearch2" placeholder="Wpisz adwersarza..." readonly>
                            </label>
                        </div>

                        <div class="input-block--column">
                          <div class="dropdown dropdown__red dropdown--small">
                              <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pozwany</button>
                              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                  <a class="dropdown-item" href="#">Action</a>
                                  <a class="dropdown-item" href="#">Another action</a>
                                  <a class="dropdown-item" href="#">Something else here</a>
                              </div>
                          </div>
                        </div>

                        <div class="input-block--column">
                          <div></div>
                        </div>

                      </div>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
            </div>
            <div class="add-new-case-content-height">
                <div class="d-flex h-100">
                    <div class="col-1 border-right-gray">
                        <div class="h-25 prosecutor-block">
                            <div class="row h-100">
                                <div class="col-12 border-bottom-gray">
                                    <div class="panel__add-new-files--prosecutor h-50 d-flex justify-content-center align-items-center">
                                        <p>powód</p>
                                    </div>
                                    <div class="panel__add-new-files--proxy panel__add-new-files--prosecutor-proxy h-50 d-flex justify-content-center align-items-center">
                                        <p>pełnomocnik</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h-25 accused-block">
                            <div class="row h-100">
                                <div class="col-12 border-bottom-gray">
                                    <div class="panel__add-new-files--accused h-50 d-flex justify-content-center align-items-center">
                                        <p>pozwany</p>
                                    </div>
                                    <div class="panel__add-new-files--proxy panel__add-new-files--accused-proxy h-50 d-flex justify-content-center align-items-center">
                                        <p>pełnomocnik</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h-25 case-data-block">
                            <div class="row h-100">
                                <div class="col-12 border-bottom-gray">
                                    <div class="panel__add-new-files--case-data h-100 d-flex justify-content-center align-items-center">
                                        <p>dane sprawy</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h-25 related-documents-block">
                            <div class="row h-100">
                                <div class="col-12">
                                    <div class="panel__add-new-files--related-documents h-100 d-flex justify-content-center align-items-center">
                                        <p>powiązane dokumenty</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-11">
                        <div class="h-25 related-documents-block">
                            <div class="row h-100">
                                <div class="col-9 border-bottom-gray">
                                    <div class="panel__add-new-files__inputs panel__add-new-files__inputs--cases-1 h-50 d-flex justify-content-start align-items-center">
                                        <div class="input-block--column">
                                            <p>nazwa</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>numer nip/pesel</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="8517688989" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>ulica</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kod pocztowy</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="71-009" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>miejscowość</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kraj</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Polska" readonly>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel__add-new-files__inputs panel__add-new-files__inputs--cases-2 h-50 d-flex justify-content-start align-items-center">
                                        <div class="input-block--column">
                                            <p>nazwa</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>numer nip/pesel</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="8517688989" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>ulica</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kod pocztowy</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="71-009" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>miejscowość</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kraj</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Polska" readonly>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3 border-bottom-gray">
                                    <div class="w-100 h-100 d-flex justify-content-center align-items-center panel__add-new-files--prosecutor-new">
                                        <p>Dodaj kolejnego</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h-25 related-documents-block">
                            <div class="row h-100">
                                <div class="col-9 border-bottom-gray">
                                    <div class="panel__add-new-files__inputs panel__add-new-files__inputs--cases-3 h-50 d-flex justify-content-start align-items-center">
                                        <div class="input-block--column">
                                            <p>nazwa</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>numer nip/pesel</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="8517688989" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>ulica</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kod pocztowy</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="71-009" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>miejscowość</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kraj</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Polska" readonly>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="panel__add-new-files__inputs panel__add-new-files__inputs--cases-4 h-50 d-flex justify-content-start align-items-center">
                                        <div class="input-block--column">
                                            <p>nazwa</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>numer nip/pesel</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="8517688989" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>ulica</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kod pocztowy</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="71-009" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>miejscowość</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                            </label>
                                        </div>
                                        <div class="input-block--column">
                                            <p>kraj</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Polska" readonly>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3 border-bottom-gray">
                                    <div class="w-100 h-100 d-flex justify-content-center align-items-center panel__add-new-files--accused-new">
                                        <p>Dodaj kolejnego</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h-25 related-documents-block">
                            <div class="row h-100">
                                <div class="col-12 border-bottom-gray">
                                    <div class="panel px-3">
                                        <div class="panel__add-new-fles w-100 h-25">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="row my-3">
                                                        <div class="col-12">
                                                            <div class="panel__add-new-files__inputs panel__add-new-files__inputs--cases-5 d-flex justify-content-start">
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Sygnatura:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="III C 650/18" readonly>
                                                                    </label>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Sąd:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Sąd Rejonowy Szczecin-Prawobrzeże i Zachód w Szczecinie" readonly>
                                                                    </label>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Wydział:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="III Wydział Cywilny" readonly>
                                                                    </label>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Referent:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="SSR Szymon Konieczny" readonly>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row my-3">
                                                        <div class="col-12">
                                                            <div class="panel__add-new-files__inputs panel__add-new-files__inputs--cases-6 d-flex justify-content-start">
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Data wpływu:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wybierz datę" readonly>
                                                                    </label>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Przedmiot sprawy:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Inne bez symbolu i o symbolu wyżej wymienionym" readonly>
                                                                    </label>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Wartość przedmiotu sprawy:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="375,00 PLN" readonly>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row my-3">
                                                        <div class="col-12">
                                                            <div class="panel__add-new-files__inputs panel__add-new-files__inputs--cases-7 d-flex justify-content-start">
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Tytuł teczki:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Maria Figat, Jan Kowalski, Inne bez symbolu i o symbolu wyżej wymienionym" readonly>
                                                                    </label>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <p>Data zakończenia:</p>
                                                                    <label for="inputSearch2" class="search-label float-left">
                                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wybierz datę" readonly>
                                                                    </label>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <div class="panel--button d-flex justify-content-end align-items-center">
                                                                        <button type="button" class="btn btn-gradient">sprawdź dane w portalu sa</button>
                                                                    </div>
                                                                </div>
                                                                <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                    <div class="panel--button d-flex justify-content-end align-items-center">
                                                                        <button type="button" class="btn btn-gradient">automatyczny wniosek dostępu do akt</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="h-25 related-documents-block">
                            <div class="row h-100">
                                <div class="col-4">
                                    <div class="panel px-3">
                                        <div class="panel__add-new-fles w-100 h-25">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="panel__add-new-files__inputs d-flex justify-content-start">
                                                        <table class="tg">
                                                            <tr>
                                                                <th class="tg-0lax">
                                                                    typ
                                                                    <img src="./img/files_addnewcase-arrow-down-ico.svg" alt="sort by type" height="10">
                                                                </th>
                                                                <th class="tg-0lax">
                                                                    opis
                                                                    <img src="./img/files_addnewcase-arrow-down-ico.svg" alt="sort by type" height="10">
                                                                </th>
                                                                <th class="tg-0lax">
                                                                    data
                                                                    <img src="./img/files_addnewcase-arrow-down-ico.svg" alt="sort by type" height="10">
                                                                </th>
                                                                <th class="tg-0lax"></th>
                                                            </tr>
                                                            <tr>
                                                                <td class="tg-0lax">
                                                <span class="actions">
                                                    czynności
                                                </span>
                                                                </td>
                                                                <td class="tg-0lax">Pierwotny wpływ sprawy</td>
                                                                <td class="tg-0lax">20 - 10 - 2017</td>
                                                                <td class="tg-0lax tg-0lax--download">
                                                                    <div></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tg-0lax">
                                                <span class="lawsuit">
                                                    pozew
                                                </span>
                                                                </td>
                                                                <td class="tg-0lax">Wysłanie pozwu w sprawie</td>
                                                                <td class="tg-0lax">20 - 10 - 2017</td>
                                                                <td class="tg-0lax tg-0lax--edit">
                                                                    <div></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tg-0lax">
                                                <span class="documents">
                                                    dokumenty
                                                </span>
                                                                </td>
                                                                <td class="tg-0lax">Wezwanie do zapłaty</td>
                                                                <td class="tg-0lax">17 - 10 - 2017</td>
                                                                <td class="tg-0lax tg-0lax--edit">
                                                                    <div></div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td class="tg-0lax">
                                                <span class="documents">
                                                    dokumenty
                                                </span>
                                                                </td>
                                                                <td class="tg-0lax">Pełnomocnictwo procesowe</td>
                                                                <td class="tg-0lax">17 - 10 - 2017</td>
                                                                <td class="tg-0lax tg-0lax--edit">
                                                                    <div></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="w-100 h-100 d-flex justify-content-start align-items-center panel__add-new-files--prosecutor-new">
                                        <p>Dodaj kolejny dokument</p>
                                    </div>
                                </div>
                                <div class="col-5">
                                    <div class="h-100">
                                        <div class="panel--button h-100 d-flex justify-content-end align-items-end">
                                            <button type="button" class="btn btn-gradient">Zapisz zmiany w adacta</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
