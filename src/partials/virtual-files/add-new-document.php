<div class="col-12" id="files-add-new-document">
        <div class="row">
            <div class="w-100 m-3 p-3 panel content-height__files--add-new-case" style="background-color:#fff">
                <div class="col-12">
                    <div class="pt-3">
                        <div class="panel__title margin-bottom-8 d-flex justify-content-between align-items-center">

                          <div class="panel__title__block">
                              <div class="panel__title__block--document-ico"></div>
                          </div>

                          <div class="input-block--column">
                              <label for="inputSearch2" class="search-label float-left">
                                  <input type="text" class="form-control input input--small input__primary" id="inputSearch2" placeholder="sygnatura" readonly>
                              </label>
                          </div>

                          <div class="input-block--column">
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz typ dokumentu</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                          </div>

                          <div class="input-block--column">
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz rodzaj dokumentu</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                          </div>

                          <div class="input-block--column">
                              <label for="inputSearch2" class="search-label float-left">
                                  <input type="text" class="form-control input input--small input__gray" id="inputSearch2" placeholder="Wpisz tytuł dokumentu" readonly>
                              </label>
                          </div>

                          <div class="input-block--column">
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz status</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                          </div>

                          <div class="input-block--column">
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Data wpływu</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                          </div>

                        </div>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
                <div class="add-new-case-content-height">
                    <div class="d-flex h-100">
                        <div class="col-12">
                            <div class="h-100 related-documents-block">
                                <div class="row h-100">
                                    <div class="col-12">
                                        <div class="h-100 d-flex flex-column justify-content-between">
                                            <div>
                                                <div class="panel__add-new-files__inputs panel__add-new-files__inputs--documents-1 d-flex justify-content-start align-items-center">

                                                    <div class="input-block--column">
                                                        <p>sygantura</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wpisz">
                                                        </label>
                                                    </div>

                                                    <div class="input-block--column">
                                                        <p>klient</p>
                                                        <div class="dropdown dropdown__gray dropdown--normal">
                                                            <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz klienta</button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <a class="dropdown-item" href="#">Action</a>
                                                                <a class="dropdown-item" href="#">Another action</a>
                                                                <a class="dropdown-item" href="#">Something else here</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="input-block--column">
                                                        <p>sprawa</p>
                                                        <div class="dropdown dropdown__gray dropdown--normal">
                                                            <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz klienta</button>
                                                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                                <a class="dropdown-item" href="#">Action</a>
                                                                <a class="dropdown-item" href="#">Another action</a>
                                                                <a class="dropdown-item" href="#">Something else here</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="input-block--column">
                                                        <p>nadawca/autor</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wpisz">
                                                        </label>
                                                    </div>

                                                    <div class="input-block--column">
                                                        <p>wymaga podjęcia czynności?</p>
                                                        <div class="radio radio__primary radio-row">
                                                            <label class="radio-label">
                                                                <input type="radio" name="optradio" checked="">
                                                                <div class="checkmark"></div>
                                                                Tak
                                                            </label>
                                                            <label class="radio-label">
                                                                <input type="radio" name="optradio">
                                                                <div class="checkmark"></div>
                                                                Nie
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div class="panel__add-new-files__inputs panel__add-new-files__inputs--documents-2 d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column">
                                                        <p>rodzaj czynności</p>
                                                        <div class="dropdown dropdown__gray dropdown--normal">
                                                           <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"   aria-expanded="false">Wybierz rodzaj czynności</button>
                                                           <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                               <a class="dropdown-item" href="#">Action</a>
                                                               <a class="dropdown-item" href="#">Another action</a>
                                                               <a class="dropdown-item" href="#">Something else here</a>
                                                           </div>
                                                        </div>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>wybierz termin</p>
                                                        <div class="dropdown dropdown__gray dropdown--normal">
                                                           <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"   aria-expanded="false">14 dni</button>
                                                           <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                               <a class="dropdown-item" href="#">Action</a>
                                                               <a class="dropdown-item" href="#">Another action</a>
                                                               <a class="dropdown-item" href="#">Something else here</a>
                                                           </div>
                                                        </div>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>wybierz datę</p>
                                                        <div class="dropdown dropdown__gray dropdown--normal">
                                                           <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"   aria-expanded="false">wybierz</button>
                                                           <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                               <a class="dropdown-item" href="#">Action</a>
                                                               <a class="dropdown-item" href="#">Another action</a>
                                                               <a class="dropdown-item" href="#">Something else here</a>
                                                           </div>
                                                        </div>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>początek</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="00:00" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>koniec</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="00:00" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            Dodaj termin do kalendarza
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__inputs panel__add-new-files__inputs--documents-3 d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column">
                                                        <p>miejsce czynności</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>sala</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>uwagi</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="" readonly>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__inputs panel__add-new-files__inputs--documents-4 d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column w-75">
                                                        <p>słowa kluczowe dokumentu <span>(wpisz dowolne słowa kluczowe oddzielając je przecinkami)</span></p>
                                                        <label for="inputSearch2" class="search-label float-left w-100">
                                                            <textarea type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly></textarea>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__inputs panel__add-new-files__inputs--documents-5 d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column w-75">
                                                        <p>dodaj opis <span>(opcjonalnie)</span></p>
                                                        <label for="inputSearch2" class="search-label float-left w-100">
                                                            <textarea type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly></textarea>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__attachment d-flex flex-column">
                                                    <p>dodaj oryginalny dokument <span>(w formacie docx, pdf lub jpg)</span></p>
                                                    <p>dodaj załączniki</p>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="panel d-flex justify-content-end align-items-end footer-block m-3">
                                                            <div class="panel--button d-flex justify-content-end align-items-center">
                                                                <button type="button" class="btn btn-gradient">zapisz zmiany w adacta</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
