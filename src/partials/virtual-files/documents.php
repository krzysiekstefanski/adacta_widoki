<div class="px-3" id="files-documents">
    <div class="my-3 shadow" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/arrow-right.svg" alt="" height="26">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Lista dokumentów</p>
                            </div>
                        </div>
                        <div class="panel__title__sort d-flex align-items-center">
                            <div class="panel__title__sort__ico panel__title__sort__ico--num ml-2"></div>
                            <div class="panel__title__sort__ico panel__title__sort__ico--lttr ml-2"></div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel content-height__files content-scroll px-3">
                    <div class="panel__task panel__task--document-list">
                        <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                            <div class="task-icon d-flex justify-content-center align-items-center">
                                <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                <p>20-09-2018</p>
                            </div>
                            <div class="panel__task__text-block panel__task__text-block--top h-100 d-flex flex-grow-1 d-md-none flex-column justify-content-end justify-content-md-center">
                                <p class="title-text text-right mr-1">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__task__document-source"></div>
                        <div class="panel__task__text-block panel__task__text-block--bottom d-flex flex-column justify-content-center">
                            <p class="title-text d-none d-md-block">Figat vs Kowalski i inni (7)</p>
                            <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                            <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                        </div>
                        <div class="panel__task__fileinfo-block d-flex justify-content-end justify-content-md-center align-items-center">
                            <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                            <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                        </div>
                    </div>
                    <div class="panel__task panel__task--document-list">
                        <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                            <div class="task-icon d-flex justify-content-center align-items-center">
                                <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                <p>20-09-2018</p>
                            </div>
                            <div class="panel__task__text-block panel__task__text-block--top h-100 d-flex flex-grow-1 d-md-none flex-column justify-content-end justify-content-md-center">
                                <p class="title-text text-right mr-1">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__task__document-source"></div>
                        <div class="panel__task__text-block panel__task__text-block--bottom d-flex flex-column justify-content-center">
                            <p class="title-text d-none d-md-block">Figat vs Kowalski i inni (7)</p>
                            <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                            <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                        </div>
                        <div class="panel__task__fileinfo-block d-flex justify-content-end justify-content-md-center align-items-center">
                            <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                            <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                        </div>
                    </div>
                    <div class="panel__task panel__task--document-list">
                        <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                            <div class="task-icon d-flex justify-content-center align-items-center">
                                <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                <p>20-09-2018</p>
                            </div>
                            <div class="panel__task__text-block panel__task__text-block--top h-100 d-flex flex-grow-1 d-md-none flex-column justify-content-end justify-content-md-center">
                                <p class="title-text text-right mr-1">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__task__document-source"></div>
                        <div class="panel__task__text-block panel__task__text-block--bottom d-flex flex-column justify-content-center">
                            <p class="title-text d-none d-md-block">Figat vs Kowalski i inni (7)</p>
                            <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                            <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                        </div>
                        <div class="panel__task__fileinfo-block d-flex justify-content-end justify-content-md-center align-items-center">
                            <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                            <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                        </div>
                    </div>
                    <div class="panel__task panel__task--document-list">
                        <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                            <div class="task-icon d-flex justify-content-center align-items-center">
                                <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                <p>20-09-2018</p>
                            </div>
                            <div class="panel__task__text-block panel__task__text-block--top h-100 d-flex flex-grow-1 d-md-none flex-column justify-content-end justify-content-md-center">
                                <p class="title-text text-right mr-1">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__task__document-source"></div>
                        <div class="panel__task__text-block panel__task__text-block--bottom d-flex flex-column justify-content-center">
                            <p class="title-text d-none d-md-block">Figat vs Kowalski i inni (7)</p>
                            <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                            <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                        </div>
                        <div class="panel__task__fileinfo-block d-flex justify-content-end justify-content-md-center align-items-center">
                            <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                            <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                        </div>
                    </div>
                    <div class="panel__task panel__task--document-list">
                        <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                            <div class="task-icon d-flex justify-content-center align-items-center">
                                <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                <p>20-09-2018</p>
                            </div>
                            <div class="panel__task__text-block panel__task__text-block--top h-100 d-flex flex-grow-1 d-md-none flex-column justify-content-end justify-content-md-center">
                                <p class="title-text text-right mr-1">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__task__document-source"></div>
                        <div class="panel__task__text-block panel__task__text-block--bottom d-flex flex-column justify-content-center">
                            <p class="title-text d-none d-md-block">Figat vs Kowalski i inni (7)</p>
                            <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                            <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                        </div>
                        <div class="panel__task__fileinfo-block d-flex justify-content-end justify-content-md-center align-items-center">
                            <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                            <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                        </div>
                    </div>
                    <div class="panel__task panel__task--document-list">
                        <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                            <div class="task-icon d-flex justify-content-center align-items-center">
                                <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                <p>20-09-2018</p>
                            </div>
                            <div class="panel__task__text-block panel__task__text-block--top h-100 d-flex flex-grow-1 d-md-none flex-column justify-content-end justify-content-md-center">
                                <p class="title-text text-right mr-1">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__task__document-source"></div>
                        <div class="panel__task__text-block panel__task__text-block--bottom d-flex flex-column justify-content-center">
                            <p class="title-text d-none d-md-block">Figat vs Kowalski i inni (7)</p>
                            <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                            <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                        </div>
                        <div class="panel__task__fileinfo-block d-flex justify-content-end justify-content-md-center align-items-center">
                            <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                            <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block px-3">
                    <div class="panel--line-grey py-3"></div>
                    <div class="panel--button d-flex justify-content-center align-items-center">
                        <button type="button" class="btn btn-white btn-outline-light add-new-document-btn">dodaj nowy dokument</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
