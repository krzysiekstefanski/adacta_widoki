<div class="w-100 d-flex justify-content-center align-items-center panel" id="settings-menu">
    <div class="panel__settings-menu" style="background-color: #fff;">
        <div class="w-100 px-3 m-auto d-flex flex-wrap justify-content-between btn-square-container">
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="1" data-name="account">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>konto</p>
            </div>
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="2" data-name="main-panel">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>panel główny</p>
            </div>
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="3" data-name="information-portal">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>api portal informacyjny</p>
            </div>
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="4" data-name="help">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>pomoc</p>
            </div>
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="5" data-name="team-managment">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>zarządzanie zespołami</p>
            </div>
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="6" data-name="calendar">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>ustawienia kalendarza</p>
            </div>
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="7" data-name="invoice">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>ustawienia fakturowania</p>
            </div>
            <div class="btn btn-square btn-square--very-small d-flex flex-column align-items-center" data-id="8" data-name="messages">
                <div class="btn-block">
                    <div class="content"></div>
                </div>
                <p>ustawienia wiadomości</p>
            </div>
        </div>
    </div>
</div>
