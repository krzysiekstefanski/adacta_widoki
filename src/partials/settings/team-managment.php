<div class="my-3 d-flex flex-column justify-content-between panel content-height__settings scroll-y-auto scroll-x-none iphone__cnt-hgt--x" id="team-managment" style="background-color: #fff;">
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Dane konta adacta</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__account content-scroll px-3">
                    <div class="row">
                        <div class="col-2">
                            <div class="panel__account__data">
                                <p>pakiet adacta</p>
                                <div class="btn btn-gradient">start</div>
                                <div class="btn btn-outline-light btn-white">zmień pakiet</div>
                            </div>
                        </div>
                        <div class="col-10 d-flex justify-content-start">
                            <div class="panel__account__data panel__account__data--uppercase">
                                <p>typ konta</p>
                                <label for="inputSearch2" class="search-label float-left">
                                    <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="indywidualne">
                                </label>
                            </div>
                            <div class="panel__account__data panel__account__data--lowercase ">
                                <p>id</p>
                                <label for="inputSearch2" class="search-label float-left">
                                    <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="annakowalska1.adacta.app">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Dane właściciela konta</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__account content-scroll px-3 d-flex justify-content-between">
                    <div class="panel__account__owner-data">
                        <p>nazwa</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Kancelaria Adwokacka Anna Kowalska" readonly>
                        </label>
                    </div>
                    <div class="panel__account__owner-data">
                        <p>numer nip/pesel</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="85072218922" readonly>
                        </label>
                    </div>
                    <div class="panel__account__owner-data">
                        <p>numer regon</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="922878676" readonly>
                        </label>
                    </div>
                    <div class="panel__account__owner-data">
                        <p>ulica i numer</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Wojska Polskiego 29/1" readonly>
                        </label>
                    </div>
                    <div class="panel__account__owner-data">
                        <p>kod pocztowy</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="71-876" readonly>
                        </label>
                    </div>
                    <div class="panel__account__owner-data">
                        <p>miejscowość</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Szczecin" readonly>
                        </label>
                    </div>
                    <div class="panel__account__owner-data">
                        <p>kraj</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Polska" readonly>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <ul class="nav nav-tabs px-1 mt-3 mx-3 mb-4">
                    <li class="nav-item"><a data-toggle="tab" role="tab" class="nav-link active" href="#users">Użytkownicy</a></li>
                    <li class="nav-item"><a data-toggle="tab" role="tab" class="nav-link" href="#invoces">Faktury</a></li>
                    <li class="nav-item"><a data-toggle="tab" role="tab" class="nav-link" href="#additional">Dodatkowe opcje</a></li>
                </ul>
                <div class="tab-content p-3">
                    <div id="users" role="tabpanel" class="tab-pane active">
                        <div class="row">
                            <div class="col-2 d-flex flex-column">
                                <p>zdjęcie</p>
                                <div class="w-100 h-100 user__block">
                                    <div class="w-100 user__avatar">
                                        <img src="https://prawnikzpolecenia.pl/wp-content/uploads/2018/06/Iwona-Zielinko-adwokat-Warszawa.jpg" alt="Avatar Użytkownika" class="img-fluid">
                                    </div>
                                </div>
                            </div>
                            <div class="col-10">
                                <div class="row">
                                    <div class="col-12 d-flex justify-content-start mb-4">
                                        <div class="panel__account__owner-data panel__account_width-inherit-from" data-inherit="1">
                                            <p>wyświetlana nazwa użytkownika</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Anna Kowalska" readonly>
                                            </label>
                                        </div>
                                        <div class="panel__account__owner-data">
                                            <p>e-mail</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="a.kowalska@adwokaci.pl" readonly>
                                            </label>
                                        </div>
                                        <div class="panel__account__owner-data">
                                            <p>telefon komórkowy</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="+48 603 657 689" readonly>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 d-flex justify-content-start">
                                        <div class="panel__account__owner-data panel__account_width-inherit-to" data-inherit="1">
                                            <p>wyświetlana funkcja</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Adwokat" readonly>
                                            </label>
                                        </div>
                                        <div class="panel__account__owner-data">
                                            <p>telefon</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="+48 91 45 678 690" readonly>
                                            </label>
                                        </div>
                                        <div class="panel__account__owner-data">
                                            <p>fax</p>
                                            <label for="inputSearch2" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder=" " readonly>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="invoces" role="tabpanel" class="tab-pane invoice-tab">
                        <div class="row">
                            <div class="col-12 panel__account__owner-data">
                                <div class="search-label float-left">
                                    <div class="form-control input input--normal input__gray d-flex justify-content-between">
                                        <p>Pakiet START abonament roczny</p>
                                        <p>18-10-2018</p>
                                        <p>800,00</p>
                                        <p>984,00</p>
                                        <p><span>opłacona</span></p>
                                        <img src="./img/download.svg" alt="ściągnij fakturę" width="24">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="additional" role="tabpanel" class="tab-pane">
                        <div class="row">
                            <div class="col-12 d-flex justify-content-start mb-4">
                                <div class="panel__account__owner-data panel__account_width-inherit-to" data-inherit="2">
                                    <p>nazwa skrócona</p>
                                    <label for="short-name" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" id="short-name" placeholder="Anna" readonly>
                                    </label>
                                </div>
                                <div class="panel__account__owner-data panel__account_width-inherit-from" data-inherit="3">
                                    <p>bank</p>
                                    <label for="bank-name" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" id="bank-name" placeholder="a.kowalska@adwokaci.pl" readonly>
                                    </label>
                                </div>
                                <div class="panel__account__owner-data">
                                    <p>nr rachunku bankowego</p>
                                    <label for="bank-account" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" id="bank-account" placeholder="+48 603 657 689" readonly>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 d-flex justify-content-start">
                                <div class="panel__account__owner-data panel__account_width-inherit-from" data-inherit="2">
                                    <p>seria dowodu osobistego</p>
                                    <label for="identity-card--series" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" id="identity-card--series" placeholder="Adwokat" readonly>
                                    </label>
                                </div>
                                <div class="panel__account__owner-data panel__account_width-inherit-to" data-inherit="3">
                                    <p>numer dowodu</p>
                                    <label for="identity-card--number" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" id="identity-card--number" placeholder="+48 91 45 678 690" readonly>
                                    </label>
                                </div>
                                <div class="panel__account__owner-data">
                                    <p>organ wydający</p>
                                    <label for="identity-card--issuer" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" id="identity-card--issuer" placeholder=" " readonly>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block m-3">
                    <div class="panel--button d-flex justify-content-end align-items-center">
                        <button type="button" class="btn btn-gradient">zapisz zmiany w adacta</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
