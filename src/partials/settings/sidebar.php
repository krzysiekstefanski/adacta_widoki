<div class="sidebar shut" id="search">
    <div class="search-panel-toggler d-flex flex-column justify-content-center align-items-center">
        <div class="circle"></div>
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="panel d-flex flex-column justify-content-between h-100">
        <div class="panel__search panel__sidemenu h-75">
            <div class="row h-100">
                <div class="col-12 py-3">
                    <div class="row">
                        <div class="col-12">
                            <div class="px-3">
                                <span class="panel__sidemenu__icon"></span>
                                <span class="form-control search-title">Opcje</span>
                            </div>
                        </div>
                    </div>
                    <div class="row h-100">
                        <div class="col-12">
                            <div class="w-100 h-25 px-3 d-flex justify-content-center btn-square-container">
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="1" data-name="account">
                                        <div class="btn-block">
                                            <div class="content active"></div>
                                        </div>
                                        <p>konto</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="2" data-name="information-portal">
                                        <div class="btn-block">
                                            <div class="content"></div>
                                        </div>
                                        <p>api portal informacyjny</p>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 h-25 px-3 d-flex justify-content-center btn-square-container">
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="3" data-name="main-panel">
                                        <div class="btn-block">
                                            <div class="content"></div>
                                        </div>
                                        <p>panel główny</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="4" data-name="help">
                                        <div class="btn-block">
                                            <div class="content"></div>
                                        </div>
                                        <p>pomoc</p>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 h-25 px-3 d-flex justify-content-center btn-square-container">
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="5" data-name="calendar">
                                        <div class="btn-block">
                                            <div class="content"></div>
                                        </div>
                                        <p>ustawienia kalendarza</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="6" data-name="invoice">
                                        <div class="btn-block">
                                            <div class="content"></div>
                                        </div>
                                        <p>ustawienia fakturowania</p>
                                    </div>
                                </div>
                            </div>
                            <div class="w-100 h-25 px-3 d-flex justify-content-center btn-square-container">
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="7" data-name="messages">
                                        <div class="btn-block">
                                            <div class="content"></div>
                                        </div>
                                        <p>ustawienia wiadomości</p>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="w-100 px-3 btn btn-square btn-square--small d-flex flex-column align-items-center" data-id="8" data-name="team-managment">
                                        <div class="btn-block">
                                            <div class="content"></div>
                                        </div>
                                        <p>zarządzanie zespołami</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel__footer h-25">
            <div class="row h-100">
                <div class="col-12 d-flex align-items-end">
                    <div class="btn btn-square btn-square--large d-flex justify-content-center align-items-center p-3" data-name="menu">
                        <div class="btn-block">
                            <div class="content d-flex justify-content-center align-items-center">
                                <div class="go-back-block">
                                    <p>powrót</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>