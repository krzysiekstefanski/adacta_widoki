<div class="my-3 d-flex flex-column justify-content-between panel content-height__settings scroll-y-auto scroll-x-none iphone__cnt-hgt--x" id="main-panel" style="background-color: #fff;">
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Wybierz układ menu</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__main-panel content-scroll px-3">
                    <div class="row">
                        <div class="d-flex flex-column px-3 panel__main-panel__radio-block">
                          <div class="radio radio__primary">
                              <label for="two-panels-layout" class="radio-label">
                                  <input type="radio" name="menu-layout" id="two-panels-layout" checked="">
                                  <div class="checkmark"></div>
                                  2-panele
                              </label>
                              <label for="four-panels-layout" class="radio-label">
                                  <input type="radio" name="menu-layout" id="four-panels-layout">
                                  <div class="checkmark"></div>
                                  4-panele
                              </label>
                          </div>
                          <div class="panel__main-panel__radio-block__preview-block d-flex">
                            <div class="panel__main-panel__radio-block__preview-block__two-panels d-flex">
                              <span class="panel__main-panel__radio-block__preview-block__two-panels__panel block--first">01</span>
                              <span class="panel__main-panel__radio-block__preview-block__two-panels__panel block--second">02</span>
                            </div>
                            <div class="panel__main-panel__radio-block__preview-block__four-panels">
                              <span class="panel__main-panel__radio-block__preview-block__four-panels__panel block--first">01</span>
                              <span class="panel__main-panel__radio-block__preview-block__four-panels__panel block--second">02</span>
                              <span class="panel__main-panel__radio-block__preview-block__four-panels__panel block--third">03</span>
                              <span class="panel__main-panel__radio-block__preview-block__four-panels__panel block--fourth">04</span>
                            </div>
                          </div>
                        </div>
                        <div class="px-3 float-left">
                            <div class="panel__addtional-info pl-5 pr-3 d-flex justify-content-between">
                                <p class="text-left">
                                  Dla małych ekranów zalecamy wybór układu składającego się z dwóch paneli dla lepszej widoczności elementów.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Pokazuj w panelu</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__main-panel px-3 d-flex flex-column flex-md-row flex-wrap">
                    <div class="d-flex panel__main-panel__show-in-main-block">
                      <div class="d-none d-md-flex flex-column justify-content-between panel__main-panel__show-in-main-block__block-id">
                        <span class="d-flex align-items-center panel__main-panel__show-in-main-block__block-id__order order--1">01</span>
                        <span class="d-flex align-items-center panel__main-panel__show-in-main-block__block-id__order order--2">02</span>
                        <span class="d-flex align-items-center panel__main-panel__show-in-main-block__block-id__order order--3">03</span>
                        <span class="d-flex align-items-center panel__main-panel__show-in-main-block__block-id__order order--4">04</span>
                        <span class="d-flex align-items-center panel__main-panel__show-in-main-block__block-id__order order--5">05</span>
                      </div>
                      <div class="d-flex flex-column justify-content-between flex-grow-1 panel__main-panel__show-in-main-block__block-panel">
                        <span class="d-flex align-items-center flex-grow-1 panel__main-panel__show-in-main-block__block-panel__order order--1">
                            <div class="checkbox checkbox__primary d-flex justify-content-start">
                                <label for="nearest-dates"class="checkbox-label">
                                    <input type="checkbox" name="show-in-dashbord" id="nearest-dates" checked>
                                    <div class="checkmark"></div>
                                </label>
                            </div>
                            Najbliższe terminy
                            <span class="grabber">
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                            </span>
                        </span>
                        <span class="d-flex align-items-center flex-grow-1 panel__main-panel__show-in-main-block__block-panel__order order--2">
                            <div class="checkbox checkbox__primary d-flex justify-content-start">
                                <label for="recently-added" class="checkbox-label">
                                    <input type="checkbox" name="show-in-dashbord" id="recently-added" checked>
                                    <div class="checkmark"></div>
                                </label>
                            </div>
                            Ostatnio dodane dokumenty
                            <span class="grabber">
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                            </span>
                        </span>
                        <span class="d-flex align-items-center flex-grow-1 panel__main-panel__show-in-main-block__block-panel__order order--3">
                            <div class="checkbox checkbox__primary d-flex justify-content-start">
                                <label for="case-in-progress" class="checkbox-label">
                                    <input type="checkbox" name="show-in-dashbord" id="case-in-progress">
                                    <div class="checkmark"></div>
                                </label>
                            </div>
                            Sprawy w toku
                            <span class="grabber">
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                            </span>
                        </span>
                        <span class="d-flex align-items-center flex-grow-1 panel__main-panel__show-in-main-block__block-panel__order order--4">
                            <div class="checkbox checkbox__primary d-flex justify-content-start">
                                <label for="recent-news" class="checkbox-label">
                                    <input type="checkbox" name="show-in-dashbord" id="recent-news">
                                    <div class="checkmark"></div>
                                </label>
                            </div>
                            Ostatnie wiadomości
                            <span class="grabber">
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                            </span>
                        </span>
                        <span class="d-flex align-items-center flex-grow-1 panel__main-panel__show-in-main-block__block-panel__order order--5">
                            <div class="checkbox checkbox__primary d-flex justify-content-start">
                                <label for="revenue-balance" class="checkbox-label">
                                    <input type="checkbox" name="show-in-dashbord" id="revenue-balance">
                                    <div class="checkmark"></div>
                                </label>
                            </div>
                            Bilans przychodów
                            <span class="grabber">
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                                <span class="grabber__item"></span>
                            </span>
                        </span>
                      </div>
                    </div>
                    <div class="px-0 px-md-3">
                        <div class="panel__addtional-info pl-5 pr-3 d-flex justify-content-between">
                            <p class="text-left">
                              Dla małych ekranów zalecamy wybór układu składającego się z dwóch paneli dla lepszej widoczności elementów.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Wybierz kolorystyke oznaczeń dokumentów</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__main-panel px-3">
                    <div class="row">
                        <div class="d-flex flex-column px-3 panel__main-panel__radio-block h-100">
                          <div class="radio radio__primary">
                              <label for="doc-colors-default" class="radio-label">
                                  <input type="radio" name="doc-colors" id="doc-colors-default" checked="">
                                  <div class="checkmark"></div>
                                  Domyślna
                              </label>
                              <label for="doc-colors-subdued" class="radio-label">
                                  <input type="radio" name="doc-colors" id="doc-colors-subdued">
                                  <div class="checkmark"></div>
                                  Stonowana
                              </label>
                          </div>
                          <div class="panel__main-panel__radio-block__color-marks d-flex">
                            <div class="panel__main-panel__radio-block__color-marks__default d-flex flex-column">
                              <span class="panel__main-panel__radio-block__color-marks__default__color block--first">Kancelaria</span>
                              <span class="panel__main-panel__radio-block__color-marks__default__color block--second">Sąd</span>
                              <span class="panel__main-panel__radio-block__color-marks__default__color block--third">Strona Przeciwna</span>
                              <span class="panel__main-panel__radio-block__color-marks__default__color block--fourth">E-Mail</span>
                            </div>
                            <div class="panel__main-panel__radio-block__color-marks__subdued d-flex flex-column">
                              <span class="panel__main-panel__radio-block__color-marks__subdued__color block--first">Kancelaria</span>
                              <span class="panel__main-panel__radio-block__color-marks__subdued__color block--second">Sąd</span>
                              <span class="panel__main-panel__radio-block__color-marks__subdued__color block--third">Strona Przeciwna</span>
                              <span class="panel__main-panel__radio-block__color-marks__subdued__color block--fourth">E-Mail</span>
                            </div>
                          </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block m-3">
                    <div class="panel--button d-flex flex-wrap flex-column flex-md-row justify-content-end align-items-center">
                        <button type="button" class="btn btn-gradient">przywróć domyślne ustawienia</button>
                        <button type="button" class="btn btn-gradient">zapisz zmiany w adacta</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>

    console.log("init func: draggable");
    $( ".panel__main-panel__show-in-main-block__block-panel" ).sortable({
        connectWith: ".panel__main-panel__show-in-main-block__block-panel__order",
        handle: ".grabber",
        placeholder: "portlet-placeholder ui-corner-all"
    });

    console.log("init func: checkmark limit");
    if ($("#two-panels-layout").is(":checked")) {
        $('input[type=checkbox]').on('change', function () {
            if ($('input[type=checkbox]:checked').length > 2) {
                $(this).prop('checked', false);
            }
        });
    } else {
        $('input[type=checkbox]').on('change', function () {
            if ($('input[type=checkbox]:checked').length > 4) {
                $(this).prop('checked', false);
            }
        });
    }

    $("input[name=menu-layout]").on('change', function () {
        if ($("#two-panels-layout").is(":checked")) {
            $('input[type=checkbox]').on('change', function () {
                if ($('input[type=checkbox]:checked').length > 2) {
                    $(this).prop('checked', false);
                }
            });
        } else {
            $('input[type=checkbox]').on('change', function () {
                if ($('input[type=checkbox]:checked').length > 4) {
                    $(this).prop('checked', false);
                }
            });
        }
    });

</script>
