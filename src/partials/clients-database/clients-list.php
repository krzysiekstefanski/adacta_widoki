<div class="w-100 shadow m-3 p-3 panel content-height__files--add-new-case active scroll-y-auto scroll-x-none iphone__cnt-hgt--x" id="clients-list" style="background-color:#fff">
    <div class="add-new-case-content-height">
        <div class="d-flex h-100">
            <div class="col-12">
                <div class="h-100 related-documents-block">
                    <div class="row h-100">
                        <div class="col-12">
                            <table class="tg tablesaw tablesaw-stack" data-tablesaw-mode="stack">
                              <thead>
                                <tr>
                                    <th class="tg-0lax">
                                        ID
                                    </th>
                                    <th class="tg-0lax">

                                    </th>
                                    <th class="tg-0lax text-left">
                                        Nazwa kilenta
                                    </th>
                                    <th class="tg-0lax">
                                        Ilość spraw
                                    </th>
                                    <th class="tg-0lax">
                                        Wygrane
                                    </th>
                                    <th class="tg-0lax">
                                        Przegrane
                                    </th>
                                    <th class="tg-0lax">
                                        W toku
                                    </th>
                                    <th class="tg-0lax">
                                        Suma faktur
                                    </th>
                                    <th class="tg-0lax text-left">
                                        Opcje
                                    </th>
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td class="tg-0lax">1</td>
                                    <td class="tg-0lax">
                                        <div class="client-avatar mx-auto">
                                            <img src="./img/pgnig-logo.png" alt="Polskie Górnictwo Naftowe i Gazownictwo" height="53">
                                        </div>
                                    </td>
                                    <td class="tg-0lax text-left">Polskie Górnictwo Naftowe i Gazownictwo</td>
                                    <td class="tg-0lax">10</td>
                                    <td class="tg-0lax">6</td>
                                    <td class="tg-0lax">2</td>
                                    <td class="tg-0lax">1</td>
                                    <td class="tg-0lax">923009,92 zł</td>
                                    <td class="tg-0lax text-left">
                                        <div class="h-100 w-100 d-flex flex-column flex-lg-row justify-content-between align-items-center">
                                            <button type="button" class="btn btn-red btn-outline-light flex-grow-1 my-1 mr-lg-2">usuń</button>
                                            <button type="button" class="btn btn-primary btn-outline-light flex-grow-1 my-1 mr-lg-2">edytuj</button>
                                            <button type="button" class="btn btn-violet btn-outline-light flex-grow-1 my-1">otwórz</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tg-0lax">2</td>
                                    <td class="tg-0lax">
                                        <div class="client-avatar mx-auto">
                                            <img src="./img/pgnig-logo.png" alt="Polskie Górnictwo Naftowe i Gazownictwo" height="53">
                                        </div>
                                    </td>
                                    <td class="tg-0lax text-left">Polskie Górnictwo Naftowe i Gazownictwo</td>
                                    <td class="tg-0lax">10</td>
                                    <td class="tg-0lax">6</td>
                                    <td class="tg-0lax">2</td>
                                    <td class="tg-0lax">1</td>
                                    <td class="tg-0lax">923009,92 zł</td>
                                    <td class="tg-0lax text-left">
                                        <div class="h-100 w-100 d-flex flex-column flex-lg-row justify-content-between align-items-center">
                                            <button type="button" class="btn btn-red btn-outline-light flex-grow-1 my-1 mr-lg-2">usuń</button>
                                            <button type="button" class="btn btn-primary btn-outline-light flex-grow-1 my-1 mr-lg-2">edytuj</button>
                                            <button type="button" class="btn btn-violet btn-outline-light flex-grow-1 my-1">otwórz</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tg-0lax">3</td>
                                    <td class="tg-0lax">
                                        <div class="client-avatar mx-auto">
                                            <img src="./img/pgnig-logo.png" alt="Polskie Górnictwo Naftowe i Gazownictwo" height="53">
                                        </div>
                                    </td>
                                    <td class="tg-0lax text-left">Polskie Górnictwo Naftowe i Gazownictwo</td>
                                    <td class="tg-0lax">10</td>
                                    <td class="tg-0lax">6</td>
                                    <td class="tg-0lax">2</td>
                                    <td class="tg-0lax">1</td>
                                    <td class="tg-0lax">923009,92 zł</td>
                                    <td class="tg-0lax text-left">
                                        <div class="h-100 w-100 d-flex flex-column flex-lg-row justify-content-between align-items-center">
                                            <button type="button" class="btn btn-red btn-outline-light flex-grow-1 my-1 mr-lg-2">usuń</button>
                                            <button type="button" class="btn btn-primary btn-outline-light flex-grow-1 my-1 mr-lg-2">edytuj</button>
                                            <button type="button" class="btn btn-violet btn-outline-light flex-grow-1 my-1">otwórz</button>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="tg-0lax">4</td>
                                    <td class="tg-0lax">
                                        <div class="client-avatar mx-auto">
                                            <img src="./img/pgnig-logo.png" alt="Polskie Górnictwo Naftowe i Gazownictwo" height="53">
                                        </div>
                                    </td>
                                    <td class="tg-0lax text-left">Polskie Górnictwo Naftowe i Gazownictwo</td>
                                    <td class="tg-0lax">10</td>
                                    <td class="tg-0lax">6</td>
                                    <td class="tg-0lax">2</td>
                                    <td class="tg-0lax">1</td>
                                    <td class="tg-0lax">923009,92 zł</td>
                                    <td class="tg-0lax text-left">
                                        <div class="h-100 w-100 d-flex flex-column flex-lg-row justify-content-between align-items-center">
                                            <button type="button" class="btn btn-red btn-outline-light flex-grow-1 my-1 mr-lg-2">usuń</button>
                                            <button type="button" class="btn btn-primary btn-outline-light flex-grow-1 my-1 mr-lg-2">edytuj</button>
                                            <button type="button" class="btn btn-violet btn-outline-light flex-grow-1 my-1">otwórz</button>
                                        </div>
                                    </td>
                                </tr>
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="./js/tablesaw-init.js"></script>
