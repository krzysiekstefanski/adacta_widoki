<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 09.10.2018
 * Time: 09:11
 */

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">

    <link rel="shortcut icon" href="img/adacta-favicon.svg" type="image/x-icon">
    <script
            src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
            crossorigin="anonymous">
    </script>
    <script src="./js/moment.js"></script>
    <script src="./js/fullcalendar.min.js"></script>
    <script src='./js/locale/pl.js'></script>

    <title>ADACTA Dashboard</title>

    <style>
        ::-webkit-scrollbar,
        ::-webkit-scrollbar-thumb,
        ::-webkit-scrollbar-track {
            width: 8px;
            border: none;
            background: #712453;
        }

        ::-webkit-scrollbar-button,
        ::-webkit-scrollbar-track-piece,
        ::-webkit-scrollbar-corner,
        ::-webkit-resizer {
            display: none;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 2px;
            background-color: white;
            border: 1px solid rgba(147, 149, 152, .3);
        }

        ::-webkit-scrollbar-track {
            background-image: radial-gradient(circle, #ffffff, #d0d0d0)
            background-size: cover;
        }
    </style>
</head>
<body class="homepage">
<div class="container-fluid">
    <div class="row">
        <div class="col-12 dashboard__block dashboard__block--top">
            <div class="row h-100">
                <div class="offset-1 col-11">
                    <div class="float-left h-100 d-flex align-items-center">
                        <img src="./img/adacta-logo-2.svg" alt="Adacta Logo" class="logo-img" height="62">
                    </div>
                    <div class="float-right h-100 d-flex align-items-center">
                        <label for="inputName1" class="search-label float-right">
                            <span class="search-label__icon search-label__icon--name"></span>
                            <input type="name" class="form-control search-label__name" id="inputName1" placeholder="szukaj">
                            <span class="search-label__checkmark"></span>
                        </label>
                        <nav class="navigation float-right">
                            <ul class="nav navbar-nav flex-md-row">
                                <a href="#"><li><p>wiadomości</p></li></a>
                                <a href="#"><li><p>opcje</p></li></a>
                                <a href="#"><li><p>wyloguj</p></li></a>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-1">
            <div class="row">
                <div class="dashboard__block dashboard__block--top-small"></div>
                <div class="dashboard__block dashboard__block--name d-flex flex-column justify-content-center align-items-center py-2">
                    <div class="w-100 h-100 user__block">
                        <div class="w-100 user__avatar">
                            <!--                                <img src="https://vignette.wikia.nocookie.net/austinally/images/1/14/Random_picture_of_shark.png" alt="Avatar Użytkownika" class="img-fluid h-100">-->
                            <img src="https://prawnikzpolecenia.pl/wp-content/uploads/2018/06/Iwona-Zielinko-adwokat-Warszawa.jpg" alt="Avatar Użytkownika" class="img-fluid h-100">
                        </div>
                    </div>
                    <span class="d-block text-center">adwokat</span>
                    <!--                        <p class="text-center">Rekin Jednorożec</p>-->
                    <p class="text-center">Anna Kowalska</p>
                </div>
            </div>
            <div class="sidebar__menu">
                <div class="row h-100 flex-column justify-content-between">
                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-1">
                        <div class="sidebar-icon icon-ico-1 new-msg" data-msg="3"></div>
                        <p class="text-center">wirtualne akta</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-2">
                        <div class="sidebar-icon icon-ico-2"></div>
                        <p class="text-center">kalendarz</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-3">
                        <div class="sidebar-icon icon-ico-3"></div>
                        <p class="text-center">baza klientów</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-4">
                        <div class="sidebar-icon icon-ico-4"></div>
                        <p class="text-center">korespondencja</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-5">
                        <div class="sidebar-icon icon-ico-5"></div>
                        <p class="text-center">faktury</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-11">
            <div class="row h-100 module">
                <div class="col-12">
                    <?php require "settings.php"; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="./js/bundle.js"></script>
</body>
</html>
