<div class="col-12 dashboard__block dashboard__block--top-small">
  <div class="float-left h-100 d-flex align-items-center">
      <img src="./img/virtual_files-ico.svg" alt="Wirtualne Akta" class="img-fluid logo-img">
  </div>
</div>
<div class="col-2 active" id="clients">
    <div class="my-3" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Lista klientów</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel content-height__files content-scroll px-3">
                    <div class="panel__client d-flex justify-content-start align-items-center">
                        <div class="panel__client__avatar-block">
                            <div class="client-avatar">
                                <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                            </div>
                        </div>
                        <div class="panel__client__text-block h-100">
                            <p class="client-name">Andryszuk Mariusz</p>
                        </div>
                    </div>
                    <div class="panel__client d-flex justify-content-start align-items-center">
                        <div class="panel__client__avatar-block">
                            <div class="client-avatar">
                                <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                            </div>
                        </div>
                        <div class="panel__client__text-block h-100">
                            <p class="client-name">Beksiński Marek</p>
                        </div>
                    </div>
                    <div class="panel__client active d-flex justify-content-start align-items-center">
                        <div class="panel__client__avatar-block">
                            <div class="client-avatar">
                                <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                            </div>
                        </div>
                        <div class="panel__client__text-block h-100">
                            <p class="client-name">Figat Maria</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block px-3">
                    <div class="panel--line-grey py-3"></div>
                    <div class="panel--button d-flex justify-content-center align-items-center">
                        <button type="button" class="btn btn-outline-light">dodaj nowego klienta</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-3" id="cases">
    <div class="my-3" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/arrow-right.svg" alt="" height="26">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Sprawy</p>
                            </div>
                        </div>
                        <div class="panel__title__sort d-flex align-items-center">
                            <div class="panel__title__sort__ico panel__title__sort__ico--num ml-2"></div>
                            <div class="panel__title__sort__ico panel__title__sort__ico--lttr ml-2"></div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel content-height__files content-scroll px-3">
                    <div class="panel__case active d-flex flex-column justify-content-center align-items-start">
                        <div class="my-2">
                            <div class="panel__case__circle-block">
                                <div class="case-circle"></div>
                            </div>
                            <div class="panel__case__text-block h-100">
                                <p class="case-name">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__case__text-block mb-2">
                            <p class="case-addons">sajkakdsj jdsknndksj</p>
                        </div>
                    </div>
                    <div class="panel__case d-flex justify-content-start align-items-center">
                        <div class="panel__case__circle-block">
                            <div class="case-circle"></div>
                        </div>
                        <div class="panel__case__text-block h-100">
                            <p class="case-name">Figat vs Kowalski i inni (7)</p>
                        </div>
                    </div>
                    <div class="panel__case d-flex justify-content-start align-items-center">
                        <div class="panel__case__circle-block">
                            <div class="case-circle"></div>
                        </div>
                        <div class="panel__case__text-block h-100">
                            <p class="case-name">Figat vs Kowalski i inni (7)</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block px-3">
                    <div class="panel--line-grey py-3"></div>
                    <div class="panel--button d-flex justify-content-center align-items-center">
                        <button type="button" class="btn btn-outline-light">dodaj nową sprawę</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-5" id="documents">
    <div class="my-3" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/arrow-right.svg" alt="" height="26">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Lista dokumentów</p>
                            </div>
                        </div>
                        <div class="panel__title__sort d-flex align-items-center">
                            <div class="panel__title__sort__ico panel__title__sort__ico--num ml-2"></div>
                            <div class="panel__title__sort__ico panel__title__sort__ico--lttr ml-2"></div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel content-height__files content-scroll px-3">
                    <div class="panel__task">
                        <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                            <div class="task-icon d-flex w-100 h-100 justify-content-center align-items-center">
                                <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                <p>20-09-2018</p>
                            </div>
                        </div>
                        <div class="panel__task__document-source"></div>
                        <div class="panel__task__text-block h-100 d-flex flex-column justify-content-center">
                            <p class="title-text">Figat vs Kowalski i inni (7)</p>
                            <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                            <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                        </div>
                        <div class="panel__task__fileinfo-block d-flex justify-content-center align-items-center">
                            <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                            <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                        </div>
                    </div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                    <div class="panel__task"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block px-3">
                    <div class="panel--line-grey py-3"></div>
                    <div class="panel--button d-flex justify-content-center align-items-center">
                        <button type="button" class="btn btn-outline-light">dodaj nowy dokument</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-5" id="preview">
    <div class="my-3" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="w-100 panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/arrow-right.svg" alt="" height="26">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Podgląd dokumentu</p>
                            </div>
                        </div>
                        <div class="d-flex">
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--zoom"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--archive"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--print"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--mail"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--lock"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--resize"></div>
                            </div>
                        </div>
                    </div>
                    <div class="w-100 panel__title margin-bottom-8 d-flex flex-column">
                        <div class="panel__title__fields panel__title__fields--up d-flex justify-content-between">
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control search-label__text" id="inputSearch2" placeholder="III C 267/17">
                            </label>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SPRAWA W TOKU</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kancelaria</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">20-09-2018</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel__title__fields panel__title__fields--down d-flex justify-content-between">
                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pismo wierzyciela</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control search-label__text" id="inputSearch2" placeholder="Wezwanie do zapłaty">
                            </label>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel content-height__files--preview content-scroll px-3"></div>
            </div>
        </div>
    </div>
</div>
<div class="col-2 search-panel-bg" id="search">
    <div class="panel d-flex flex-column justify-content-between h-100">
        <div class="panel__search">
            <div class="row">
                <div class="col-12 py-3">
                    <div class="row">
                        <div class="col-12">
                            <span class="search-label__icon search-label__icon--text"></span>
                            <span class="form-control search-title">Wyszukiwanie</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <p class="dropdown-text">szukaj w</p>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-1">
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control search-label__text" id="inputSearch2" placeholder="Wpisz szukaną frazę...">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 py-3">
                    <div class="row">
                        <div class="col-12">
                            <span class="filter-label__icon filter-label__icon--text"></span>
                            <span class="form-control search-title">Filtrowanie</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 mt-3 mb-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <div class="dropdown w-100">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszyscy klienci</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <div class="dropdown w-100">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszystkie sprawy</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 my-1">
                            <div class="h-100 d-flex justify-content-between align-items-center">
                                <div class="dropdown w-100">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszystkie typy dokumentów</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pb-3">
                    <p>Zakres dat</p>
                    <div class="w-50 float-left">
                        <div class="h-100 d-flex justify-content-between align-items-center pr-1">
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control search-label__text" data-toggle="datepicker" id="inputSearch2" placeholder="Od">
                            </label>
                        </div>
                    </div>
                    <div class="w-50 float-left">
                        <div class="h-100 d-flex justify-content-between align-items-center pl-1">
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control search-label__text" data-toggle="datepicker" id="inputSearch2" placeholder="Do">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 pb-3">
                    <p>Źródła dokumentów:</p>
                    <div class="radio">
                        <label class="radio-label">
                            <input type="radio" name="optradio" checked>
                            <div class="checkmark"></div>
                            Wszystkie źródła
                        </label>

                        <label class="radio-label">
                            <input type="radio" name="optradio">
                            <div class="checkmark"></div>
                            Tylko Portal Informacyjny
                        </label>

                        <label class="radio-label">
                            <input type="radio" name="optradio">
                            <div class="checkmark"></div>
                            Tylko AdActa
                        </label>

                        <label class="radio-label">
                            <input type="radio" name="optradio">
                            <div class="checkmark"></div>
                            Archiwum
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel__footer">
            <div class="row">
                <div class="col-12">
                    <div class="add-panel-button add-panel-button--small d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="content"></div>
                        </div>
                    </div>
                    <div class="add-panel-button add-panel-button--small d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="content"></div>
                        </div>
                    </div>
                    <div class="add-panel-button add-panel-button--small d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="content"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="add-panel-button add-panel-button--large d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="content d-flex justify-content-center align-items-center">
                                <div class="go-back-block">
                                    <p>powrót</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

