<div class="px-3" id="files-preview">
    <div class="my-3 shadow scroll-none" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="w-100 panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/arrow-right.svg" alt="" height="26">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Podgląd dokumentu</p>
                            </div>
                        </div>
                        <div class="d-none d-md-flex">
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--zoom"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--archive"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--print"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--mail"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--lock"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--resize"></div>
                            </div>
                        </div>
                        <div class="panel__title__arrow-down panel__title__arrow-down--big d-block d-md-none">

                        </div>
                    </div>

                    <div class="w-100 panel__title panel__title__dropdown-panel margin-bottom-8 d-flex flex-column">
                          <div class="d-flex d-md-none my-3">
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--zoom"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--archive"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--print"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--mail"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--lock"></div>
                            </div>
                            <div class="panel__title__prev-settings d-flex align-items-center">
                                <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--resize"></div>
                            </div>
                        </div>
                        <div class="panel__title__fields panel__title__fields--up d-flex flex-column flex-md-row justify-content-between">

                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control input input--small input__primary" id="inputSearch2" placeholder="III C 267/17">
                            </label>
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SPRAWA W TOKU</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kancelaria</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">20-09-2018</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                        </div>
                        <div class="panel__title__fields panel__title__fields--down d-flex flex-column flex-md-row justify-content-between">
                            <div class="dropdown dropdown__primary dropdown--small">
                                <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pismo wierzyciela</button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Action</a>
                                    <a class="dropdown-item" href="#">Another action</a>
                                    <a class="dropdown-item" href="#">Something else here</a>
                                </div>
                            </div>
                            <label for="inputSearch2" class="search-label float-right">
                                <input type="text" class="form-control input input--small input__primary" id="inputSearch2" placeholder="Wezwanie do zapłaty">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="px-3">
                    <div class="panel--line-grey"></div>
                </div>
                <div class="panel content-height__files--preview content-scroll px-3">
                    <div class="preview-img h-100 w-100">
                        <img class="img-fluid" src="img/document.png" alt="podgląd dokumentu">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
