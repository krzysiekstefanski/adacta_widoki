<div class="px-3" id="files-cases">
    <div class="my-3 shadow" style="background-color: #fff;">
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/arrow-right.svg" alt="" height="26">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Sprawy</p>
                            </div>
                        </div>
                        <div class="panel__title__sort d-flex align-items-center">
                            <div class="panel__title__sort__ico panel__title__sort__ico--num ml-2"></div>
                            <div class="panel__title__sort__ico panel__title__sort__ico--lttr ml-2"></div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel content-height__files content-scroll px-3">
                    <div class="panel__case active d-flex flex-column justify-content-center align-items-start">
                        <div class="my-2">
                            <div class="panel__case__circle-block">
                                <div class="case-circle"></div>
                            </div>
                            <div class="panel__case__text-block h-100">
                                <p class="case-name">Figat vs Kowalski i inni (7)</p>
                            </div>
                        </div>
                        <div class="panel__case__text-block mb-2">
                            <p class="case-addons">sajkakdsj jdsknndksj</p>
                        </div>
                    </div>
                    <div class="panel__case d-flex justify-content-start align-items-center">
                        <div class="panel__case__circle-block">
                            <div class="case-circle"></div>
                        </div>
                        <div class="panel__case__text-block h-100">
                            <p class="case-name">Figat vs BZWBK</p>
                        </div>
                    </div>
                    <div class="panel__case d-flex justify-content-start align-items-center">
                        <div class="panel__case__circle-block">
                            <div class="case-circle"></div>
                        </div>
                        <div class="panel__case__text-block h-100">
                            <p class="case-name">Figat vs Skrzyński</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block px-3">
                    <div class="panel--line-grey py-3"></div>
                    <div class="panel--button d-flex justify-content-center align-items-center">
                        <button type="button" class="btn btn-white btn-outline-light add-new-case-btn">dodaj nową sprawę</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
