<div class="my-3 d-flex flex-column justify-content-between panel content-height__settings scroll-y-auto scroll-x-none iphone__cnt-hgt--x" id="help" style="background-color: #fff;">
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Kontakt</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__form">
                    <div class="panel__form__help content-scroll px-3 d-flex flex-row justify-content-start">
                        <div class="pr-1 panel__form__help__data">
                            <p>Wybierz dział pomocy</p>
                            <label for="helpDesk" class="w-100 search-label float-left">
                                <input type="text" class="form-control input input--normal input__gray" id="helpDesk" placeholder="wybierz z listy" readonly>
                            </label>
                        </div>
                        <div class="pl-1 panel__form__help__data">
                            <p>Wybierz temat</p>
                            <label for="helpTopic" class="w-100 search-label float-left">
                                <input type="text" class="form-control input input--normal input__gray" id="helpTopic" placeholder="wybierz z listy" readonly>
                            </label>
                        </div>
                    </div>
                    <div class="panel__form__help content-scroll px-3 d-flex flex-row justify-content-start">
                        <div class="panel__form__help__data">
                            <p>Wpisz treść zapytania</p>
                            <label for="helpDesc" class="w-100 search-label float-left">
                                <textarea type="text" class="form-control input input--normal input__gray" id="helpDesc"></textarea>
                            </label>
                        </div>
                    </div>
                    <div class="panel__form__help px-3">
                        <button type="button" class="btn btn-gradient mt-5">wyślij</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Najczęstsze pytania i problemy</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__help px-3">
                    <button type="button" class="btn btn-gradient">przejdź do sekcji faq</button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Instrukcje</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__form__help">
                    <ul class="panel__form__help__list">
                        <li class="panel__form__help__list--video">
                            <a href="#">Zobacz film z instrukcją obsługi AdActa</a>
                        </li>
                        <li class="panel__form__help__list--pdf">
                            <a href="#">Pobierz instrukcję PDF dla aplikacji AdActa</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Regulamin i polityka prywatności</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__form__help">
                    <ul class="panel__form__help__list">
                        <li class="panel__form__help__list--privacy-policy">
                            <a href="#">Polityka prywatności AdActa</a>
                        </li>
                        <li class="panel__form__help__list--terms">
                            <a href="#">Regulamin korzystania z aplikacji AdActa</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
