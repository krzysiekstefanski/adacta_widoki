<div class="my-3 d-flex flex-column justify-content-between panel content-height__settings scroll-y-auto scroll-x-none iphone__cnt-hgt--x" id="calendar" style="background-color: #fff;">
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Terminy</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__calendar content-scroll px-3">
                    <div class="row">
                        <div class="d-flex flex-column px-3 panel__calendar__radio-block panel__calendar__radio-block--terms">
                            <div class="radio radio__primary">
                                <label for="terms-automatically" class="radio-label">
                                    <input type="radio" name="terms" id="terms-automatically" checked="">
                                    <div class="checkmark"></div>
                                    Pozwalaj na automatyczne dodawanie terminów
                                    z portalu informacyjnego
                                </label>
                                <label for="terms-manual" class="radio-label">
                                    <input type="radio" name="terms" id="terms-manual">
                                    <div class="checkmark"></div>
                                    Dodawaj terminy tylko ręcznie
                                </label>
                            </div>
                        </div>
                        <div class="px-3 float-left">
                            <div class="panel__addtional-info pl-5 pr-3 d-flex justify-content-between">
                                <p class="text-left">
                                    Dla wygody i pełnego korzystania z AdActa zalecamy zezwolenie na korzystanie
                                    z terminów publikowanych i pobieranych z portalu informacyjnego. System będzie
                                    Cię informował, gdy wystąpi prawdopodobieństwo zduplikowania terminu
                                    w kalendarzu.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Kolorystyka i kategorie kalendarza</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="panel panel__calendar px-3">
                    <div class="row">
                        <div class="d-flex flex-wrap flex-column flex-md-row px-3 panel__calendar__radio-block h-100">
                            <div class="d-flex flex-column my-4">
                                <div class="radio radio__primary">
                                    <label for="color-default" class="radio-label w-100">
                                        <input type="radio" name="calendar-colors" id="color-default" checked="">
                                        <div class="checkmark"></div>
                                        Domyślna paleta
                                    </label>
                                </div>
                                <div class="panel__calendar__radio-block__color-checkbox d-flex">
                                    <div class="panel__calendar__radio-block__color-color-checkbox checkbox--default d-flex flex-column w-100">
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Kancelaria</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-1" class="radio-label">
                                                    <input type="radio" name="color-blue" id="color-blue-1">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-1" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-red-1" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-1" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-purple-1">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-1" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-green-1">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-1" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-yellow-1">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-1" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-orange-1">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Sąd</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-2" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-blue-2">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-2" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-red-2" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-2" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-purple-2">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-2" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-green-2">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-2" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-yellow-2">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-2" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-orange-2">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Spotkania</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-3" class="radio-label">
                                                    <input type="radio" name="color-blue" id="color-blue-3">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-3" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-red-3" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-3" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-purple-3">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-3" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-green-3">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-3" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-yellow-3">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-3" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-orange-3">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Aktywności</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-4" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-blue-4">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-4" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-red-4" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-4" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-purple-4">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-4" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-green-4">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-4" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-yellow-4">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-4" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-orange-4">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="d-flex flex-column my-4">
                                <div class="radio radio__primary">
                                    <label for="subdued-colors" class="radio-label w-100">
                                        <input type="radio" name="calendar-colors" id="subdued-colors">
                                        <div class="checkmark"></div>
                                        Stonowana paleta
                                    </label>
                                </div>
                                <div class="panel__calendar__radio-block__color-checkbox d-flex">
                                    <div class="panel__calendar__radio-block__color-color-checkbox checkbox--subdued d-flex flex-column w-100">
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Kancelaria</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-5" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-blue-5">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-5" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-red-5" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-5" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-purple-5">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-5" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-green-5">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-5" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-yellow-5">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-5" class="radio-label">
                                                    <input type="radio" name="color-office" id="color-orange-5">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Sąd</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-6" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-blue-6">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-6" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-red-6" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-6" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-purple-6">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-6" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-green-6">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-6" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-yellow-6">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-6" class="radio-label">
                                                    <input type="radio" name="color-court" id="color-orange-6">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Spotkania</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-7" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-blue-7">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-7" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-red-7" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-7" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-purple-7">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-7" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-green-7">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-7" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-yellow-7">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-7" class="radio-label">
                                                    <input type="radio" name="color-meeting" id="color-orange-7">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100 d-flex justify-content-start justify-content-sm-between align-items-center">
                                            <span class="panel__calendar__radio-block__color-checkbox__default__color block--first">Aktywności</span>
                                            <div class="radio radio__primary d-flex justify-content-start">
                                                <label for="color-blue-8" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-blue-8">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-red-8" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-red-8" checked="checked">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-purple-8" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-purple-8">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-green-8" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-green-8">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-yellow-8" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-yellow-8">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <label for="color-orange-8" class="radio-label">
                                                    <input type="radio" name="color-activity" id="color-orange-8">
                                                    <div class="checkmark"></div>
                                                </label>
                                                <div>
                                                    <p>Zmień kolor</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block m-3">
                    <div class="panel--button d-flex flex-wrap justify-content-end align-items-center">
                        <button type="button" class="btn btn-gradient">przywróć domyślne ustawienia</button>
                        <button type="button" class="btn btn-gradient">zapisz zmiany w adacta</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="overlay"></div>

<script>
    if ($(window).width() <= 576) {
        $(".panel__calendar__radio-block__color-checkbox").find(".radio-label").hide();
        $(".panel__calendar__radio-block__color-checkbox").find("input:checked").parent().show();
    } else {
        $(".panel__calendar__radio-block__color-checkbox").find(".radio-label").show();
    }

    $(window).resize(function() {
        if ($(window).width() <= 576 && !$(".overlay").hasClass("active")) {
            $(".panel__calendar__radio-block__color-checkbox").find(".radio-label").hide();
            $(".panel__calendar__radio-block__color-checkbox").find("input:checked").parent().show();
        } else {
            $(".panel__calendar__radio-block__color-checkbox").find(".radio-label").show();
        }
    });

    $(".panel__calendar__radio-block__color-checkbox").find("p").click(function() {
        $(this).parent().parent().children(".radio-label").find("input:checked").parent().show();
        $(".overlay").addClass("active");
        $(this).parent().parent().find("p").hide();
        $(this).parent().parent().addClass("active");
        $(this).parent().parent().addClass("justify-content-center");
        $(this).parent().parent().children(".radio-label").show();

        $(".overlay, .radio.active .checkmark").click(function() {
            $(".radio.active").children(".radio-label").hide();
            $(".radio.active").children(".radio-label").find("input:checked").parent().show();
            console.log($(".radio.active").children(".radio-label").find("input:checked").parent());
            $(".radio.active").find("p").show();
            $(".overlay").removeClass("active");
            $(".radio.active").removeClass("justify-content-center");
            $(".radio.active").removeClass("active");
        });
    });


</script>
