<div class="my-3 d-flex flex-column justify-content-between panel content-height__settings scroll-y-auto scroll-x-none iphone__cnt-hgt--x" id="information-portal" style="background-color: #fff;">
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel p-3">
                    <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                        <div class="d-flex align-items-center">
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Logowanie do portalu informacyjnego</p>
                            </div>
                        </div>
                    </div>
                    <div class="panel--line-grey"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="px-3 float-left">
                <div class="panel panel__information content-scroll px-3 d-flex flex-column justify-content-between">
                    <div class="panel__information__data">
                        <p>nazwa</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Kancelaria Adwokacka Anna Kowalska" readonly>
                        </label>
                    </div>
                    <div class="panel__information__data">
                        <p>numer nip/pesel</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="85072218922" readonly>
                        </label>
                    </div>
                    <div class="panel__information__data">
                        <p>numer regon</p>
                        <label for="inputSearch2" class="search-label float-left">
                            <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="922878676" readonly>
                        </label>
                    </div>
                    <button type="button" class="btn btn-gradient mt-5">powiąż konto z adacta</button>
                </div>
            </div>
            <div class="px-3 float-left">
                <div class="panel__addtional-info pl-5 pr-3 d-flex justify-content-between">
                    <p class="text-left">
                        Powiązanie profilu z dowolnego portalu informacyjnego sądów administracyjnych z AdActa pozwala
                        na bieżący dostęp do wszystkich dokumentów publikowanych przez sądy i powiązanych z Twoimi
                        sprawami.
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div>
        <div class="row">
            <div class="col-12">
                <div class="panel footer-block m-3">
                    <div class="panel--button d-flex justify-content-end align-items-center">
                        <button type="button" class="btn btn-gradient">zapisz zmiany w adacta</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
