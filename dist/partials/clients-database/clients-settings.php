<div class="w-100 shadow m-3 p-3 panel content-height__files--add-new-case flex-column justify-content-between panel scroll-y-auto scroll-x-none iphone__cnt-hgt--x" id="clients-settings" style="background-color: #fff;">
    <div class="d-flex flex-column justify-content-between h-100">
        <div class="panel__form form-clients-databse accordion">
            <ul>
              <li class="accordion-item">
                <div>
                  <div class="d-flex d-md-none justify-content-between py-3 arrow-1">
                    <p>Dane klienta</p>
                    <div class="panel__title__arrow-down panel__title__arrow-down--small d-block d-md-none"></div>
                  </div>
                  <div class="screen-1 active">
                    <div class="row">
                        <div class="col-12">
                            <div class="panel panel__clients-databse content-scroll px-3 mb-3">
                                <div class="row">
                                    <div class="px-0 px-md-3 d-flex flex-wrap w-100 justify-content-start">
                                        <div class="d-flex align-items-center">
                                            <div class="panel__clients-databse__data">
                                                <div class="btn btn-gradient">pobierz dane z rejestru gus</div>
                                            </div>
                                        </div>
                                        <div class="d-flex flex-wrap justify-content-start align-items-center">
                                          <div class="panel__clients-databse__owner-data panel__clients-databse__data--uppercase">
                                              <p>typ klienta</p>
                                              <div class="radio">
                                                  <label for="ct1" class="radio-label">
                                                      <input type="radio" name="client-type" id="ct1" checked>
                                                      <div class="checkmark"></div>
                                                      Firma
                                                  </label>
                                                  <label for="ct2" class="radio-label">
                                                      <input type="radio" name="client-type" id="ct2">
                                                      <div class="checkmark"></div>
                                                      Indywidualny
                                                  </label>
                                              </div>
                                          </div>
                                          <div class="panel__clients-databse__owner-data panel__clients-databse__data--lowercase ">
                                              <p>id</p>
                                              <label for="client-id" class="search-label float-left">
                                                  <input type="text" class="form-control input input__gray" name="client-id" id="client-id" placeholder="11">
                                              </label>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="pl-md-3 float-left">
                            <div class="panel panel__clients-databse content-scroll px-3 mb-3 d-flex flex-wrap justify-content-start">
                                <div class="panel__clients-databse__owner-data">
                                    <p>nazwa klienta</p>
                                    <label for="client-name" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" name="client-name" id="client-name" placeholder="Polskie Grnictwo Naftowe i Gazownictwo" readonly>
                                    </label>
                                </div>
                                <div class="panel__clients-databse__owner-data">
                                    <p>numer nip/pesel</p>
                                    <label for="client-nip" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" name="client-nip" id="client-nip" placeholder="8517688989" readonly>
                                    </label>
                                </div>
                                <div class="panel__clients-databse__owner-data">
                                    <p>numer regon</p>
                                    <label for="client-regon" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" name="client-regon" id="client-regon" placeholder="8517688" readonly>
                                    </label>
                                </div>
                            </div>
                            <div class="panel panel__clients-databse content-scroll px-3 mb-3 d-flex flex-wrap justify-content-start">
                                <div class="panel__clients-databse__owner-data">
                                    <p>ulica i numer</p>
                                    <label for="client-street" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" name="client-street" id="client-street" placeholder="Naftowa" readonly>
                                    </label>
                                </div>
                                <div class="panel__clients-databse__owner-data">
                                    <p>kod pocztowy</p>
                                    <label for="client-postcode" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" name="client-postcode" id="client-postcode" placeholder="00-210" readonly>
                                    </label>
                                </div>
                                <div class="panel__clients-databse__owner-data">
                                    <p>miejscowość</p>
                                    <label for="client-city" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" name="client-city" id="client-city" placeholder="Warszawa" readonly>
                                    </label>
                                </div>
                                <div class="panel__clients-databse__owner-data">
                                    <p>kraj</p>
                                    <label for="client-country" class="search-label float-left">
                                        <input type="text" class="form-control input input--normal input__gray" name="client-country" id="client-country" placeholder="Polska" readonly>
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="panel__form form-clients-databse__logo-block float-left d-flex flex-column">
                            <p class="add-logo-text">dodaj logo</p>
                            <div class="avatar__block avatar__block--panel mx-auto mx-md-0">
                                <div class="avatar__block__image-container avatar__block__image-container--panel">
                                    <img src="https://prawnikzpolecenia.pl/wp-content/uploads/2018/06/Iwona-Zielinko-adwokat-Warszawa.jpg" alt="Avatar Użytkownika" class="img-fluid">
                                </div>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </li>
                <div>
                  <div>
                    <div class="row">
                      <div class="col-12">

                        <ul class="nav nav-tabs px-1 mt-3 mx-3 mb-4">
                            <li class="nav-item"><a data-toggle="tab" role="tab" class="nav-link active" href="#additional-settings">Opcje dodatkowe</a></li>
                            <li class="nav-item"><a data-toggle="tab" role="tab" class="nav-link" href="#addresses">Adresy</a></li>
                            <li class="nav-item"><a data-toggle="tab" role="tab" class="nav-link" href="#contact-persons">Osoby kontaktowe</a></li>
                        </ul>

                        <div class="tab-content p-0 p-md-3">
                          <li class="accordion-item">
                            <div class="d-flex d-md-none justify-content-between py-3 arrow-1 border-top-gray">
                              <p>Opcje dodatkowe</p>
                              <div class="panel__title__arrow-down panel__title__arrow-down--small d-block d-md-none"></div>
                            </div>
                            <div id="additional-settings" role="tabpanel" class="tab-pane screen-1 active">
                              <div class="row">
                                  <div class="col-12 d-flex flex-wrap justify-content-start mb-4">
                                      <div class="panel__clients-databse__owner-data panel__clients-databse_width-inherit-from" data-inherit="1">
                                          <p>nazwa skrócona</p>
                                          <label for="client-shortname" class="search-label float-left">
                                              <input type="text" class="form-control input input--normal input__gray" name="client-shortname" id="client-shortname" placeholder="PGNiG" readonly>
                                          </label>
                                      </div>
                                      <div class="panel__clients-databse__owner-data">
                                          <p>bank</p>
                                          <label for="client-bank" class="search-label float-left">
                                              <input type="text" class="form-control input input--normal input__gray" name="client-bank" id="client-bank" placeholder="mBank" readonly>
                                          </label>
                                      </div>
                                      <div class="panel__clients-databse__owner-data">
                                          <p>nr rachunku bankowego</p>
                                          <label for="client-bankacc" class="search-label float-left">
                                              <input type="text" class="form-control input input--normal input__gray" name="client-bankacc" id="client-bankacc" placeholder="90 4040 2004 2004 0000 8989 9078">
                                          </label>
                                      </div>
                                  </div>
                              </div>
                              <div class="row">
                                  <div class="col-12 d-flex flex-wrap justify-content-start">
                                      <div class="panel__clients-databse__owner-data panel__clients-databse_width-inherit-to" data-inherit="1">
                                          <p>seria dowodu osobistego</p>
                                          <label for="client-idcardseries" class="search-label float-left">
                                              <input type="text" class="form-control input input--normal input__gray" name="client-idcardseries" id="client-idcardseries" placeholder=" " readonly>
                                          </label>
                                      </div>
                                      <div class="panel__clients-databse__owner-data">
                                          <p>numer dowodu</p>
                                          <label for="client-idcardnumber" class="search-label float-left">
                                              <input type="text" class="form-control input input--normal input__gray" name="client-idcardnumber" id="client-idcardnumber" placeholder=" " readonly>
                                          </label>
                                      </div>
                                      <div class="panel__clients-databse__owner-data">
                                          <p>organ wydający</p>
                                          <label for="client-idcardissuing" class="search-label float-left">
                                              <input type="text" class="form-control input input--normal input__gray" name="client-idcardissuing" id="client-idcardissuing" placeholder=" " readonly>
                                          </label>
                                      </div>
                                  </div>
                              </div>
                            </div>
                          </li>
                          <li class="accordion-item">
                            <div class="d-flex d-md-none justify-content-between py-3 arrow-1 border-top-gray">
                              <p>Adresy</p>
                              <div class="panel__title__arrow-down panel__title__arrow-down--small d-block d-md-none"></div>
                            </div>
                            <div id="addresses" role="tabpanel" class="tab-pane screen-1">
                                <div class="row">
                                    <div class="col-12 d-none d-md-flex flex-column flex-md-row justify-content-start">
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p>nazwa adresu</p>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p>ulica i numer</p>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p>kod pocztowy</p>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p>miejscowość</p>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p>kraj</p>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column justify-content-end" style="width: 130px"></div>
                                    </div>
                                    <div class="col-12 d-flex flex-column flex-md-row justify-content-start">
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p class="d-block d-md-none">nazwa adresu</p>
                                            <label for="addresses_client-address" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" name="client-address" id="addresses_client-address" placeholder="Adwokat" readonly>
                                            </label>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p class="d-block d-md-none">ulica i numer</p>
                                            <label for="addresses_client-street" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" name="client-street" id="addresses_client-street" placeholder="+48 91 45 678 690" readonly>
                                            </label>
                                        </div>
                                         <div class="panel__clients-databse__owner-data d-flex flex-column">
                                           <p class="d-block d-md-none">kod pocztowy</p>
                                            <label for="addresses_client-postcode" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" name="client-postcode" id="addresses_client-postcode" placeholder=" " readonly>
                                            </label>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p class="d-block d-md-none">miejscowość</p>
                                            <label for="addresses_client-city" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" name="client-city" id="addresses_client-city" placeholder=" " readonly>
                                            </label>
                                        </div>
                                        <div class="panel__clients-databse__owner-data d-flex flex-column">
                                            <p class="d-block d-md-none">kraj</p>
                                            <label for="addresses_client-country" class="search-label float-left">
                                                <input type="text" class="form-control input input--normal input__gray" name="client-country" id="addresses_client-country" placeholder=" " readonly>
                                            </label>
                                        </div>
                                        <div class="panel__clients-databse__owner-data add-new-address active d-flex flex-column justify-content-center">
                                            <div class="d-flex align-items-center">
                                                <p>dodaj kolejny</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </li>
                          <li class="accordion-item">
                            <div class="d-flex d-md-none justify-content-between py-3 arrow-1 border-top-gray">
                              <p>Osoby kontaktowe</p>
                              <div class="panel__title__arrow-down panel__title__arrow-down--small d-block d-md-none"></div>
                            </div>
                            <div id="contact-persons" role="tabpanel" class="tab-pane screen-1">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="row">
                                            <div class="col-12 d-none d-md-flex flex-column flex-md-row justify-content-start">
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <p>nazwa adresu</p>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <p>ulica i numer</p>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <p>kod pocztowy</p>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <p>miejscowość</p>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <p>kraj</p>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column justify-content-end" style="width: 130px"></div>
                                            </div>
                                            <div class="col-12 d-flex flex-column flex-md-row justify-content-start">
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <label for="inputSearch2" class="search-label float-left">
                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="Adwokat" readonly>
                                                    </label>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <label for="inputSearch2" class="search-label float-left">
                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder="+48 91 45 678 690" readonly>
                                                    </label>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <label for="inputSearch2" class="search-label float-left">
                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder=" " readonly>
                                                    </label>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <label for="inputSearch2" class="search-label float-left">
                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder=" " readonly>
                                                    </label>
                                                </div>
                                                <div class="panel__clients-databse__owner-data d-flex flex-column">
                                                    <label for="inputSearch2" class="search-label float-left">
                                                        <input type="text" class="form-control input input--normal input__gray" id="inputSearch2" placeholder=" " readonly>
                                                    </label>
                                                </div>
                                                <div class="panel__clients-databse__owner-data add-new-address active d-flex flex-column justify-content-center">
                                                    <div class="d-flex align-items-center">
                                                        <p>dodaj kolejny</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </li>
                        </div>
                    </div>
                    </div>
                  </div>
                </div>
            </ul>
        </div>
        <div>
            <div class="row">
                <div class="col-12">
                    <div class="panel footer-block mx-0 my-0 mb-3 m-md-3">
                        <div class="panel--button d-flex justify-content-start align-items-center">
                            <p>dodaj załączniki</p>
                        </div>
                        <div class="panel--button d-flex flex-wrap flex-column flex-md-row justify-content-end align-items-center">
                            <button type="button" class="btn btn-gradient">generuj pełnomocnictwo</button>
                            <button type="button" class="btn btn-gradient">zapisz kilenta w adacta</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>

if ($(window).width() < 768) {
  $(".accordion .accordion-item .screen-1").hide();
    $('.arrow-1').first().addClass("plus");
    $('.screen-1').first().show().addClass("plus");
} else {
  $(".accordion .accordion-item .screen-1.active").show();
}

$( window ).resize(function() {
  if ($(window).width() < 768) {
    $(".accordion .accordion-item .screen-1").hide();
      $('.arrow-1').first().addClass("plus");
      $('.screen-1').first().show().addClass("plus");
  } else {
    $(".accordion .accordion-item .screen-1.active").show();
  }
});

$(".accordion .accordion-item .arrow-1").click(function () {
  var current_li = $(this).parent();
  console.log(current_li);
  $(".accordion .accordion-item .screen-1").each(function(i,el) {
    if($(el).parent().is(current_li)) {
      $(el).prev().toggleClass("plus");
      $(el).slideToggle();
    } else{
      $(el).prev().removeClass("plus");
      $(el).slideUp();
    }
  });
});

</script>
