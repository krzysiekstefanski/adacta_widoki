<div class="sidebar shut" id="clients-database-sidebar">
    <div class="search-panel-toggler d-flex flex-column justify-content-center align-items-center">
        <div class="circle"></div>
        <div class="circle"></div>
        <div class="circle"></div>
    </div>
    <div class="panel d-flex flex-column justify-content-between h-100 px-3 scroll-y-auto scroll-x-none">
        <div class="panel__search">
            <div class="row">
                <div class="col-12">
                    <div class="btn btn-square btn-square--large d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="content d-flex justify-content-center align-items-center">
                                <div class="add-new-client-block">
                                    <p>dodaj nowego klienta</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 py-3">
                    <div class="row">
                        <div class="col-12">
                            <span class="filter-label__icon filter-label__icon--text"></span>
                            <span class="form-control search-title">Sortowanie i Filtrowanie</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 pb-3">
                            <div class="radio">
                                <label class="radio-label">
                                    <input type="radio" name="optradio" checked>
                                    <div class="checkmark"></div>
                                    Sortuj od A do Z
                                </label>

                                <label class="radio-label">
                                    <input type="radio" name="optradio">
                                    <div class="checkmark"></div>
                                    Sortuj od Z do A
                                </label>

                                <label class="radio-label">
                                    <input type="radio" name="optradio">
                                    <div class="checkmark"></div>
                                    Po numerze ID rosnąco
                                </label>

                                <label class="radio-label">
                                    <input type="radio" name="optradio">
                                    <div class="checkmark"></div>
                                    Po numerze ID malejąco
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="w-100 px-4 d-none d-md-block">
                    <p>Ilość spraw</p>
                    <div id="slider-range-1"></div>
                    <p>Ilość wygranych spraw</p>
                    <div id="slider-range-2"></div>
                    <p>Ilość przegranych spraw</p>
                    <div id="slider-range-3"></div>
                    <p>Suma faktur z przedziału</p>
                    <div id="slider-range-4"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <span class="search-label__icon search-label__icon--text"></span>
                    <span class="form-control search-title">Wyszukiwanie</span>
                </div>
            </div>
            <div class="row">
                <div class="col-12 my-1">
                    <label for="inputSearch" class="search-label float-right">
                        <input type="text" class="form-control input-white" id="inputSearch" placeholder="Wpisz szukaną frazę...">
                    </label>
                </div>
            </div>
        </div>
        <div class="panel__footer">
            <div class="row">
                <div class="col-12">
                    <div class="btn btn-square btn-square--large d-flex justify-content-center align-items-center">
                        <div class="btn-block">
                            <div class="content go-back-btn d-flex justify-content-center align-items-center">
                                <div class="go-back-block">
                                    <p>powrót</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>