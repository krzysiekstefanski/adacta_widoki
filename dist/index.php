<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 09.10.2018
 * Time: 09:11
 */

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, viewport-fit=cover">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700" rel="stylesheet">
    <link rel="stylesheet" href="css/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/tablesaw.stackonly.css">
    <link class="style-general" rel="stylesheet" href="css/main.css">
    <link class="style-module" rel="stylesheet" href="css/virtual-files.css">

    <link rel="shortcut icon" href="img/adacta-favicon.svg" type="image/x-icon">

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="//code.jquery.com/mobile/1.5.0-alpha.1/jquery.mobile-1.5.0-alpha.1.min.js"></script>
    <script src="js/jquery-ui.js"></script>
    <script src="./js/datepicker.min.js"></script>
    <script src="./js/moment.js"></script>
    <script src="./js/fullcalendar.min.js"></script>
    <script src='./js/locale/pl.js'></script>
    <script src="./js/tablesaw.stackonly.jquery.js"></script>

    <title>ADACTA Dashboard</title>

    <style>
        ::-webkit-scrollbar,
        ::-webkit-scrollbar-thumb,
        ::-webkit-scrollbar-track {
            width: 8px;
            border: none;
            background: rgba(147, 149, 152, .1);
        }

        ::-webkit-scrollbar-button,
        ::-webkit-scrollbar-track-piece,
        ::-webkit-scrollbar-corner,
        ::-webkit-resizer {
            display: none;
        }

        ::-webkit-scrollbar-thumb {
            border-radius: 5px;
            background-color: #712453;
            border: 1px solid rgba(147, 149, 152, .3);
        }

        ::-webkit-scrollbar-track {
            background-image: radial-gradient(circle, #ffffff, #d0d0d0)
            background-size: cover;
        }
    </style>
</head>
<body class="homepage">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 order-1 d-none d-md-block dashboard__block dashboard__block--top">
                <div class="row h-100">
                    <div class="avatar__block avatar__block--dashboard px-3 py-1">
                        <div class="avatar__block__image-container avatar__block__image-container--dashboard m-auto">
                            <img src="https://prawnikzpolecenia.pl/wp-content/uploads/2018/06/Iwona-Zielinko-adwokat-Warszawa.jpg" alt="Avatar Użytkownika" class="img-fluid">
                        </div>
                    </div>
                    <div class="d-flex navigation__block px-3">
                        <div class="float-left h-100 d-flex flex-grow-1 p-2 align-items-center">
                            <img src="./img/adacta-logo-2.svg" alt="Adacta Logo" class="logo-img img-fluid h-100 d-block">
                        </div>
                        <div class="float-right h-100 d-flex align-items-center">
                            <label for="inputName1" class="search-label float-right">
                                <span class="search-label__icon search-label__icon--name"></span>
                                <input type="name" class="form-control search-label__name" id="inputName1" placeholder="szukaj">
                                <span class="search-label__checkmark"></span>
                            </label>
                            <nav class="navigation float-right">
                                <ul class="nav navbar-nav flex-md-row justify-content-end">
                                    <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                                        <a class="navbar-nav__item__link navbar-nav__item__link--messages d-flex justify-content-center align-items-center" href="#">
                                            <p>wiadomości</p>
                                            <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--messages"></div>
                                        </a>
                                    </li>
                                    <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                                        <a class="navbar-nav__item__link navbar-nav__item__link--settings d-flex justify-content-center align-items-center" href="#">
                                            <p>opcje</p>
                                            <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--settings"></div>
                                        </a>
                                    </li>
                                    <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                                        <a class="navbar-nav__item__link navbar-nav__item__link--logout d-flex justify-content-center align-items-center" href="#">
                                            <p>wyloguj</p>
                                            <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--logout"></div>
                                        </a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <div class="order-3 order-md-2 category-menu px-3">
                <div class="sidebar__menu">
                  <div class="row h-100 flex-row flex-md-column justify-content-between">
                   <div class="dashboard__block dashboard__block--regular dashboard__block--name d-none d-md-flex flex-column justify-content-center align-items-center py-2">
                        <span class="d-block text-center">adwokat</span>
                        <p class="text-center">Anna Kowalska</p>
                    </div>
                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-1">
                        <div class="sidebar-icon icon-ico-1 new-msg" data-msg="3"></div>
                        <p class="text-center d-none d-md-block">wirtualne akta</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-2">
                        <div class="sidebar-icon icon-ico-2"></div>
                        <p class="text-center d-none d-md-block">kalendarz</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-3">
                        <div class="sidebar-icon icon-ico-3"></div>
                        <p class="text-center d-none d-md-block">baza klientów</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-4">
                        <div class="sidebar-icon icon-ico-4"></div>
                        <p class="text-center d-none d-md-block">korespondencja</p>
                    </div>

                    <div class="dashboard__block dashboard__block--regular d-flex flex-column justify-content-center align-items-center" id="sidebar-5">
                        <div class="sidebar-icon icon-ico-5"></div>
                        <p class="text-center d-none d-md-block">faktury</p>
                    </div>
                  </div>
                </div>
            </div>
            <div class="order-2 order-md-3 content-area px-0 px-md-3">
                <div class="h-100 module">
                    <div class="col-12">
                        <?php require "main.php"; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="./js/bundle.js"></script>
</body>
</html>
