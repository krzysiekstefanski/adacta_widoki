<div class="row">
    <div class="col-12 dashboard__block dashboard__block--top-small">
        <div class="float-left h-100 w-25 d-flex align-items-center">
            <div class="logo-img__block">
                <div class="logo-img__block--square">
                    <img src="./img/ico-1.svg" alt="Wirtualne Akta" class="img-fluid logo-img">
                </div>
            </div>
            <div class="logo-text__block">
                <p>wirtualne akta</p>
            </div>
        </div>
    </div>
</div>
<div class="row flex-nowrap">
    <div class="col-12 d-block" id="files-add-new-document">
        <div class="row">
            <div class="w-100 m-3 p-3 panel content-height__files--add-new-case" style="background-color:#fff">
                <div class="row">
                    <div class="col-12">
                        <div class="pt-3">
                            <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                                <div class="d-flex align-items-center">
                                    <div class="panel__title__block">
                                        <div class="panel__title__block--case-ico"></div>
                                    </div>
                                    <div class="input-block--column">
                                        <label for="inputSearch2" class="search-label float-left">
                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                        </label>
                                    </div>
                                    <div class="input-block--column">
                                        <label for="inputSearch2" class="search-label float-left">
                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                        </label>
                                    </div>
                                    <div class="input-block--column">
                                        <label for="inputSearch2" class="search-label float-left">
                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                        </label>
                                    </div>
                                    <div class="input-block--column">
                                        <label for="inputSearch2" class="search-label float-left">
                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                        </label>
                                    </div>
                                    <div class="input-block--column">
                                        <label for="inputSearch2" class="search-label float-left">
                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                        </label>
                                    </div>
                                    <div class="input-block--column">
                                        <label for="inputSearch2" class="search-label float-left">
                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="panel--line-grey"></div>
                        </div>
                    </div>
                </div>
                <div class="add-new-case-content-height">
                    <div class="d-flex h-100">
                        <div class="col-12">
                            <div class="h-100 related-documents-block">
                                <div class="row h-100">
                                    <div class="col-12">
                                        <div class="h-100 d-flex flex-column justify-content-between">
                                            <div>
                                                <div class="panel__add-new-files__inputs d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column">
                                                        <p>sygantura</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>klient</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="8517688989" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>sprawa</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>nadawca/autor</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="71-009" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>wymaga podjęcia czynności?</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__inputs d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column">
                                                        <p>rodzaj czynności</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>wybierz termin</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="8517688989" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>wybierz datę</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>początek</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="71-009" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>koniec</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>dodaj termin do kalendarza</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Polska" readonly>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__inputs d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column">
                                                        <p>miejsce czynności</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>sala</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="8517688989" readonly>
                                                        </label>
                                                    </div>
                                                    <div class="input-block--column">
                                                        <p>uwagi</p>
                                                        <label for="inputSearch2" class="search-label float-left">
                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__inputs d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column w-75">
                                                        <p>słowa kluczowe dokumentu <span>(wpisz dowolne słowa kluczowe oddzielając je przecinkami)</span></p>
                                                        <label for="inputSearch2" class="search-label float-left w-100">
                                                            <textarea type="text" class="form-control input-gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly></textarea>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__inputs d-flex justify-content-start align-items-center">
                                                    <div class="input-block--column w-75">
                                                        <p>dodaj opis <span>(opcjonalnie)</span></p>
                                                        <label for="inputSearch2" class="search-label float-left w-100">
                                                            <textarea type="text" class="form-control input-gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly></textarea>
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="panel__add-new-files__attachment d-flex flex-column">
                                                    <p>dodaj oryginalny dokument (w formacie docx, pdf lub jpg)</p>
                                                    <p>dodaj załączniki</p>
                                                </div>
                                            </div>
                                            <div>
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="panel footer-block m-3">
                                                            <div class="panel--button d-flex justify-content-end align-items-center">
                                                                <button type="button" class="btn btn-gradient">zapisz zmiany w adacta</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-12 d-block" id="files-add-new-case">
        <div class="row">
            <div class="w-100 m-3 p-3 panel content-height__files--add-new-case" style="background-color:#fff">
                <div class="row">
                    <div class="col-12">
                        <div class="pt-3">
                            <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                                <div class="d-flex align-items-center">
                                    <div class="panel__title__block">
                                        <div class="panel__title__block--case-ico"></div>
                                    </div>
                                    <div class="panel__title__block">
                                        <p class="panel__title__block title-text">Dane właściciela konta</p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel--line-grey"></div>
                        </div>
                    </div>
                </div>
                <div class="add-new-case-content-height">
                    <div class="d-flex h-100">
                        <div class="col-1 border-right-gray">
                            <div class="h-25 prosecutor-block">
                                <div class="row h-100">
                                    <div class="col-12 border-bottom-gray">
                                        <div class="panel__add-new-files--prosecutor h-50 d-flex justify-content-center align-items-center">
                                            <p>powód</p>
                                        </div>
                                        <div class="panel__add-new-files--proxy panel__add-new-files--prosecutor-proxy h-50 d-flex justify-content-center align-items-center">
                                            <p>pełnomocnik</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="h-25 accused-block">
                                <div class="row h-100">
                                    <div class="col-12 border-bottom-gray">
                                        <div class="panel__add-new-files--accused h-50 d-flex justify-content-center align-items-center">
                                            <p>pozwany</p>
                                        </div>
                                        <div class="panel__add-new-files--proxy panel__add-new-files--accused-proxy h-50 d-flex justify-content-center align-items-center">
                                            <p>pełnomocnik</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="h-25 case-data-block">
                                <div class="row h-100">
                                    <div class="col-12 border-bottom-gray">
                                        <div class="panel__add-new-files--case-data h-100 d-flex justify-content-center align-items-center">
                                            <p>dane sprawy</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="h-25 related-documents-block">
                                <div class="row h-100">
                                    <div class="col-12">
                                        <div class="panel__add-new-files--related-documents h-100 d-flex justify-content-center align-items-center">
                                            <p>powiązane dokumenty</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-11">
                            <div class="h-25 related-documents-block">
                                <div class="row h-100">
                                    <div class="col-9 border-bottom-gray">
                                        <div class="panel__add-new-files__inputs h-50 d-flex justify-content-start align-items-center">
                                            <div class="input-block--column">
                                                <p>nazwa</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>numer nip/pesel</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="8517688989" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>ulica</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kod pocztowy</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="71-009" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>miejscowość</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kraj</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Polska" readonly>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="panel__add-new-files__inputs h-50 d-flex justify-content-start align-items-center">
                                            <div class="input-block--column">
                                                <p>nazwa</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>numer nip/pesel</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="8517688989" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>ulica</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kod pocztowy</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="71-009" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>miejscowość</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kraj</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Polska" readonly>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 border-bottom-gray">
                                        <div class="w-100 h-100 d-flex justify-content-center align-items-center panel__add-new-files--prosecutor-new">
                                            <p>Dodaj kolejnego</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="h-25 related-documents-block">
                                <div class="row h-100">
                                    <div class="col-9 border-bottom-gray">
                                        <div class="panel__add-new-files__inputs h-50 d-flex justify-content-start align-items-center">
                                            <div class="input-block--column">
                                                <p>nazwa</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>numer nip/pesel</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="8517688989" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>ulica</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kod pocztowy</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="71-009" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>miejscowość</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kraj</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Polska" readonly>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="panel__add-new-files__inputs h-50 d-flex justify-content-start align-items-center">
                                            <div class="input-block--column">
                                                <p>nazwa</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Wybierz adwokata" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>numer nip/pesel</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="8517688989" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>ulica</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Tumska 12/2" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kod pocztowy</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="71-009" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>miejscowość</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Szczecin" readonly>
                                                </label>
                                            </div>
                                            <div class="input-block--column">
                                                <p>kraj</p>
                                                <label for="inputSearch2" class="search-label float-left">
                                                    <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Polska" readonly>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3 border-bottom-gray">
                                        <div class="w-100 h-100 d-flex justify-content-center align-items-center panel__add-new-files--accused-new">
                                            <p>Dodaj kolejnego</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="h-25 related-documents-block">
                                <div class="row h-100">
                                    <div class="col-12 border-bottom-gray">
                                        <div class="panel px-3">
                                            <div class="panel__add-new-fles w-100 h-25">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="row my-3">
                                                            <div class="col-12">
                                                                <div class="panel__add-new-files__inputs d-flex justify-content-start">
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Sygnatura:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="III C 650/18" readonly>
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Sąd:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Sąd Rejonowy Szczecin-Prawobrzeże i Zachód w Szczecinie" readonly>
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Wydział:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="III Wydział Cywilny" readonly>
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Referent:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="SSR Szymon Konieczny" readonly>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row my-3">
                                                            <div class="col-12">
                                                                <div class="panel__add-new-files__inputs d-flex justify-content-start">
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Data wpływu:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Wybierz datę" readonly>
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Przedmiot sprawy:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Inne bez symbolu i o symbolu wyżej wymienionym" readonly>
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Wartość przedmiotu sprawy:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="375,00 PLN" readonly>
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row my-3">
                                                            <div class="col-12">
                                                                <div class="panel__add-new-files__inputs d-flex justify-content-start">
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Tytuł teczki:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder="Maria Figat, Jan Kowalski, Inne bez symbolu i o symbolu wyżej wymienionym" readonly>
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <p>Data zakończenia:</p>
                                                                        <label for="inputSearch2" class="search-label float-left">
                                                                            <input type="text" class="form-control input-gray" id="inputSearch2" placeholder=" " readonly>
                                                                        </label>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <div class="panel--button d-flex justify-content-end align-items-center">
                                                                            <button type="button" class="btn btn-gradient">sprawdź dane w portalu sa</button>
                                                                        </div>
                                                                    </div>
                                                                    <div class="input-block--row d-flex justify-content-start align-items-center">
                                                                        <div class="panel--button d-flex justify-content-end align-items-center">
                                                                            <button type="button" class="btn btn-gradient">automatyczny wniosek dostępu do akt</button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="h-25 related-documents-block">
                                <div class="row h-100">
                                    <div class="col-4">
                                        <div class="panel px-3">
                                            <div class="panel__add-new-fles w-100 h-25">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="panel__add-new-files__inputs d-flex justify-content-start">
                                                            <table class="tg">
                                                                <tr>
                                                                    <th class="tg-0lax">
                                                                        typ
                                                                        <img src="./img/files_addnewcase-arrow-down-ico.svg" alt="sort by type" height="10">
                                                                    </th>
                                                                    <th class="tg-0lax">
                                                                        opis
                                                                        <img src="./img/files_addnewcase-arrow-down-ico.svg" alt="sort by type" height="10">
                                                                    </th>
                                                                    <th class="tg-0lax">
                                                                        data
                                                                        <img src="./img/files_addnewcase-arrow-down-ico.svg" alt="sort by type" height="10">
                                                                    </th>
                                                                    <th class="tg-0lax"></th>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tg-0lax">
                                                    <span class="actions">
                                                        czynności
                                                    </span>
                                                                    </td>
                                                                    <td class="tg-0lax">Pierwotny wpływ sprawy</td>
                                                                    <td class="tg-0lax">20 - 10 - 2017</td>
                                                                    <td class="tg-0lax">ico</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tg-0lax">
                                                    <span class="lawsuit">
                                                        pozew
                                                    </span>
                                                                    </td>
                                                                    <td class="tg-0lax">Wysłanie pozwu w sprawie</td>
                                                                    <td class="tg-0lax">20 - 10 - 2017</td>
                                                                    <td class="tg-0lax">ico</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tg-0lax">
                                                    <span class="documents">
                                                        dokumenty
                                                    </span>
                                                                    </td>
                                                                    <td class="tg-0lax">Wezwanie do zapłaty</td>
                                                                    <td class="tg-0lax">17 - 10 - 2017</td>
                                                                    <td class="tg-0lax">ico</td>
                                                                </tr>
                                                                <tr>
                                                                    <td class="tg-0lax">
                                                    <span class="documents">
                                                        dokumenty
                                                    </span>
                                                                    </td>
                                                                    <td class="tg-0lax">Pełnomocnictwo procesowe</td>
                                                                    <td class="tg-0lax">17 - 10 - 2017</td>
                                                                    <td class="tg-0lax">ico</td>
                                                                </tr>
                                                            </table>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <div class="w-100 h-100 d-flex justify-content-center align-items-center panel__add-new-files--prosecutor-new">
                                            <p>Dodaj kolejny dokument</p>
                                        </div>
                                    </div>
                                    <div class="col-5">
                                        <div class="h-100">
                                            <div class="panel--button h-100 d-flex justify-content-end align-items-end">
                                                <button type="button" class="btn btn-gradient">Zapisz zmiany w adacta</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="col-2 active" id="files-clients">
        <div class="my-3" style="background-color: #fff;">
            <div class="row">
                <div class="col-12">
                    <div class="panel p-3">
                        <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <div class="panel__title__block">
                                    <p class="panel__title__block title-text">Lista klientów</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel content-height__files content-scroll px-3">
                        <div class="panel__client d-flex justify-content-start align-items-center">
                            <div class="panel__client__avatar-block">
                                <div class="client-avatar">
                                    <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                                </div>
                            </div>
                            <div class="panel__client__text-block h-100">
                                <p class="client-name">Beksiński Marek</p>
                            </div>
                        </div>
                        <div class="panel__client active d-flex justify-content-start align-items-center">
                            <div class="panel__client__avatar-block">
                                <div class="client-avatar">
                                    <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                                </div>
                            </div>
                            <div class="panel__client__text-block h-100">
                                <p class="client-name">Figat Maria</p>
                            </div>
                        </div>
                        <div class="panel__client d-flex justify-content-start align-items-center">
                            <div class="panel__client__avatar-block">
                                <div class="client-avatar">
                                    <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                                </div>
                            </div>
                            <div class="panel__client__text-block h-100">
                                <p class="client-name">Andryszuk Mariusz</p>
                            </div>
                        </div>
                        <div class="panel__client d-flex justify-content-start align-items-center">
                            <div class="panel__client__avatar-block">
                                <div class="client-avatar">
                                    <img src="./img/default-client-ico.svg" alt="domyślny avatar klienta" height="28">
                                </div>
                            </div>
                            <div class="panel__client__text-block h-100">
                                <p class="client-name">Andryszuk Marianna</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel footer-block px-3">
                        <div class="panel--line-grey py-3"></div>
                        <div class="panel--button d-flex justify-content-center align-items-center">
                            <button type="button" class="btn btn-white btn-outline-light">dodaj nowego klienta</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-3" id="files-cases">
        <div class="my-3" style="background-color: #fff;">
            <div class="row">
                <div class="col-12">
                    <div class="panel p-3">
                        <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <div class="panel__title__icon">
                                    <img src="./img/arrow-right.svg" alt="" height="26">
                                </div>
                                <div class="panel__title__block">
                                    <p class="panel__title__block title-text">Sprawy</p>
                                </div>
                            </div>
                            <div class="panel__title__sort d-flex align-items-center">
                                <div class="panel__title__sort__ico panel__title__sort__ico--num ml-2"></div>
                                <div class="panel__title__sort__ico panel__title__sort__ico--lttr ml-2"></div>
                            </div>
                        </div>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel content-height__files content-scroll px-3">
                        <div class="panel__case active d-flex flex-column justify-content-center align-items-start">
                            <div class="my-2">
                                <div class="panel__case__circle-block">
                                    <div class="case-circle"></div>
                                </div>
                                <div class="panel__case__text-block h-100">
                                    <p class="case-name">Figat vs Kowalski i inni (7)</p>
                                </div>
                            </div>
                            <div class="panel__case__text-block mb-2">
                                <p class="case-addons">sajkakdsj jdsknndksj</p>
                            </div>
                        </div>
                        <div class="panel__case d-flex justify-content-start align-items-center">
                            <div class="panel__case__circle-block">
                                <div class="case-circle"></div>
                            </div>
                            <div class="panel__case__text-block h-100">
                                <p class="case-name">Figat vs BZWBK</p>
                            </div>
                        </div>
                        <div class="panel__case d-flex justify-content-start align-items-center">
                            <div class="panel__case__circle-block">
                                <div class="case-circle"></div>
                            </div>
                            <div class="panel__case__text-block h-100">
                                <p class="case-name">Figat vs Skrzyński</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel footer-block px-3">
                        <div class="panel--line-grey py-3"></div>
                        <div class="panel--button d-flex justify-content-center align-items-center">
                            <button type="button" class="btn btn-white btn-outline-light">dodaj nową sprawę</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5" id="files-documents">
        <div class="my-3" style="background-color: #fff;">
            <div class="row">
                <div class="col-12">
                    <div class="panel p-3">
                        <div class="panel__title margin-bottom-8 d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <div class="panel__title__icon">
                                    <img src="./img/arrow-right.svg" alt="" height="26">
                                </div>
                                <div class="panel__title__block">
                                    <p class="panel__title__block title-text">Lista dokumentów</p>
                                </div>
                            </div>
                            <div class="panel__title__sort d-flex align-items-center">
                                <div class="panel__title__sort__ico panel__title__sort__ico--num ml-2"></div>
                                <div class="panel__title__sort__ico panel__title__sort__ico--lttr ml-2"></div>
                            </div>
                        </div>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel content-height__files content-scroll px-3">
                        <div class="panel__task">
                            <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                                <div class="task-icon d-flex w-100 h-100 justify-content-center align-items-center">
                                    <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                    <p>20-09-2018</p>
                                </div>
                            </div>
                            <div class="panel__task__document-source"></div>
                            <div class="panel__task__text-block h-100 d-flex flex-column justify-content-center">
                                <p class="title-text">Figat vs Kowalski i inni (7)</p>
                                <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                                <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                            </div>
                            <div class="panel__task__fileinfo-block d-flex justify-content-center align-items-center">
                                <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                                <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                            </div>
                        </div>
                        <div class="panel__task">
                            <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                                <div class="task-icon d-flex w-100 h-100 justify-content-center align-items-center">
                                    <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                    <p>20-09-2018</p>
                                </div>
                            </div>
                            <div class="panel__task__document-source"></div>
                            <div class="panel__task__text-block h-100 d-flex flex-column justify-content-center">
                                <p class="title-text">Figat vs Kowalski i inni (7)</p>
                                <p class="address-text">Rozpisanie terminu/wykonanie zarządzenia</p>
                                <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                            </div>
                            <div class="panel__task__fileinfo-block d-flex justify-content-center align-items-center">
                                <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                                <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                            </div>
                        </div>
                        <div class="panel__task">
                            <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                                <div class="task-icon d-flex w-100 h-100 justify-content-center align-items-center">
                                    <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                    <p>20-09-2018</p>
                                </div>
                            </div>
                            <div class="panel__task__document-source"></div>
                            <div class="panel__task__text-block h-100 d-flex flex-column justify-content-center">
                                <p class="title-text">Figat vs Kowalski i inni (7)</p>
                                <p class="address-text">Wyznaczenie sędziego</p>
                                <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                            </div>
                            <div class="panel__task__fileinfo-block d-flex justify-content-center align-items-center">
                                <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                                <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel footer-block px-3">
                        <div class="panel--line-grey py-3"></div>
                        <div class="panel--button d-flex justify-content-center align-items-center">
                            <button type="button" class="btn btn-white btn-outline-light">dodaj nowy dokument</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-5" id="files-preview">
        <div class="my-3" style="background-color: #fff;">
            <div class="row">
                <div class="col-12">
                    <div class="panel p-3">
                        <div class="w-100 panel__title margin-bottom-8 d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <div class="panel__title__icon">
                                    <img src="./img/arrow-right.svg" alt="" height="26">
                                </div>
                                <div class="panel__title__block">
                                    <p class="panel__title__block title-text">Podgląd dokumentu</p>
                                </div>
                            </div>
                            <div class="d-flex">
                                <div class="panel__title__prev-settings d-flex align-items-center">
                                    <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--zoom"></div>
                                </div>
                                <div class="panel__title__prev-settings d-flex align-items-center">
                                    <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--archive"></div>
                                </div>
                                <div class="panel__title__prev-settings d-flex align-items-center">
                                    <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--print"></div>
                                </div>
                                <div class="panel__title__prev-settings d-flex align-items-center">
                                    <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--mail"></div>
                                </div>
                                <div class="panel__title__prev-settings d-flex align-items-center">
                                    <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--lock"></div>
                                </div>
                                <div class="panel__title__prev-settings d-flex align-items-center">
                                    <div class="panel__title__prev-settings__ico panel__title__prev-settings__ico--resize"></div>
                                </div>
                            </div>
                        </div>
                        <div class="w-100 panel__title margin-bottom-8 d-flex flex-column">
                            <div class="panel__title__fields panel__title__fields--up d-flex justify-content-between">
                                <label for="inputSearch2" class="search-label float-right">
                                    <input type="text" class="form-control search-label__text" id="inputSearch2" placeholder="III C 267/17">
                                </label>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">SPRAWA W TOKU</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Kancelaria</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">20-09-2018</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                            </div>
                            <div class="panel__title__fields panel__title__fields--down d-flex justify-content-between">
                                <div class="dropdown">
                                    <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pismo wierzyciela</button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="#">Action</a>
                                        <a class="dropdown-item" href="#">Another action</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                    </div>
                                </div>
                                <label for="inputSearch2" class="search-label float-right">
                                    <input type="text" class="form-control search-label__text" id="inputSearch2" placeholder="Wezwanie do zapłaty">
                                </label>
                            </div>
                        </div>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel content-height__files--preview content-scroll px-3"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-2 content-height__tiles" id="files-tiles">
        <div class="h-100 d-flex flex-column justify-content-between" style="padding-top: 15px; padding-bottom: 15px">
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-prosecutor-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__block">
                                    <p class="panel__tile__block tile-text">Powód</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel--line-grey"></div>
                        <div class="panel__tile">
                            <div class="panel__tile__text">
                                <ul>
                                    <li>Maria Figat</li>
                                    <li>ul. Tumska 12/2, 71-009 Dobra</li>
                                </ul>
                                <p class="mt-2"><span>Pełnomocnik</span></p>
                                <ul>
                                    <li>Kancelaria Adwokacka Anna Kowalska,</li>
                                    <li>ul. Ks. Bogusława X 12/3</li>
                                    <li>71-809 Szczecin</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-accused-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__block">
                                    <p class="panel__tile__block tile-text red">Pozwany</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel--line-grey"></div>
                        <div class="panel__tile">
                            <div class="panel__tile__text">
                                <ul>
                                    <li>Jan Kowalski</li>
                                    <li>ul. Chojecka 9, 71-009 Dobra</li>
                                </ul>
                                <p class="mt-2"><span class="red">Pełnomocnik</span></p>
                                <ul>
                                    <li>Kancelaria Adwokacka Jan Nadolny,</li>
                                    <li>ul. Juranda 12/3</li>
                                    <li>71-856 Szczecin</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-subcject-matter-value-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__text">
                                    <p><span>Wartość przedmiotu sprawy:</span> 375,00 zł</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-referent-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__text">
                                    <p class="panel__tile__block tile-text"><span>Referent:</span> SSR Szymon Konieczny</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-court-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__text">
                                    <p><span>Sąd:</span></p>
                                    <p>Sąd Rejonowy Szczecin-Prawobrzeże i Zachód w Szczecinie</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-deparment-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__text">
                                    <p class="panel__tile__block tile-text"><span>Wydział:</span> III Wydział Cywilny</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-subcject-matter-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__text">
                                    <p><span>Przedmiot sprawy:</span></p>
                                    <p>Inne bez symbolu i o symbolu wyżej wymienionym</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-briefcase-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__text">
                                    <p><span>Tytuł teczki:</span></p>
                                    <p>Maria Figat , Jan Kowalski, Inne bez symbolu i o symbolu wyżej wymienionym</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel py-1 px-2" style="background-color: #fff;">
                        <div class="panel__tile d-flex justify-content-between">
                            <div class="d-flex align-items-center">
                                <div class="panel__tile__icon">
                                    <img src="./img/tile-history-ico.svg" alt="" height="14">
                                </div>
                                <div class="panel__tile__block">
                                    <p class="panel__tile__block tile-text">Historia zmian:</p>
                                </div>
                            </div>
                        </div>
                        <div class="panel--line-grey"></div>
                        <div class="row">
                            <div class="col-11 panel__tile content-height__history content-scroll">
                                <div class="panel__tile__history">
                                    <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                    <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                    <div class="panel--line-grey"></div>
                                </div>
                                <div class="panel__tile__history">
                                    <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                    <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                    <div class="panel--line-grey"></div>
                                </div>
                                <div class="panel__tile__history">
                                    <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                    <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                    <div class="panel--line-grey"></div>
                                </div>
                                <div class="panel__tile__history">
                                    <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                    <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                    <div class="panel--line-grey"></div>
                                </div>
                                <div class="panel__tile__history">
                                    <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                    <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                    <div class="panel--line-grey"></div>
                                </div>
                                <div class="panel__tile__history">
                                    <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                    <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                    <div class="panel--line-grey"></div>
                                </div>
                                <div class="panel__tile__history">
                                    <p class="date"><span >24-09-2018 | 11:08</span> @Anna_Kowalska</p>
                                    <p class="source">pobrano dokument <span>wzwdo.pdf</span></p>
                                    <div class="panel--line-grey"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-2 search-panel-bg shut" id="files-search">
        <div class="search-panel-toggler d-flex flex-column justify-content-center align-items-center">
            <div class="circle"></div>
            <div class="circle"></div>
            <div class="circle"></div>
        </div>
        <div class="panel d-flex flex-column justify-content-between h-100">
            <div class="panel__search">
                <div class="row">
                    <div class="col-12 py-3">
                        <div class="row">
                            <div class="col-12">
                                <span class="search-label__icon search-label__icon--text"></span>
                                <span class="form-control search-title">Wyszukiwanie</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-3 mb-1">
                                <div class="h-100 d-flex justify-content-between align-items-center">
                                    <p class="dropdown-text">szukaj w</p>
                                    <div class="dropdown">
                                        <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wybierz</button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 my-1">
                                <label for="inputSearch2" class="search-label float-right">
                                    <input type="text" class="form-control input-white" id="inputSearch2" placeholder="Wpisz szukaną frazę...">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 py-3">
                        <div class="row">
                            <div class="col-12">
                                <span class="filter-label__icon filter-label__icon--text"></span>
                                <span class="form-control search-title">Filtrowanie</span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 mt-3 mb-1">
                                <div class="h-100 d-flex justify-content-between align-items-center">
                                    <div class="dropdown w-100">
                                        <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszyscy klienci</button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 my-1">
                                <div class="h-100 d-flex justify-content-between align-items-center">
                                    <div class="dropdown w-100">
                                        <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszystkie sprawy</button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12 my-1">
                                <div class="h-100 d-flex justify-content-between align-items-center">
                                    <div class="dropdown w-100">
                                        <button class="btn btn-secondary dropdown-toggle w-100" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Wszystkie typy dokumentów</button>
                                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pb-3">
                        <p>Zakres dat</p>
                        <div class="w-50 float-left">
                            <div class="h-100 d-flex justify-content-between align-items-center pr-1">
                                <label for="inputSearch2" class="search-label float-right">
                                    <input type="text" class="form-control input-white" data-toggle="datepicker" id="inputSearch2" placeholder="Od">
                                </label>
                            </div>
                        </div>
                        <div class="w-50 float-left">
                            <div class="h-100 d-flex justify-content-between align-items-center pl-1">
                                <label for="inputSearch2" class="search-label float-right">
                                    <input type="text" class="form-control input-white" data-toggle="datepicker" id="inputSearch2" placeholder="Do">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 pb-3">
                        <p>Źródła dokumentów:</p>
                        <div class="radio">
                            <label class="radio-label">
                                <input type="radio" name="optradio" checked>
                                <div class="checkmark"></div>
                                Wszystkie źródła
                            </label>

                            <label class="radio-label">
                                <input type="radio" name="optradio">
                                <div class="checkmark"></div>
                                Tylko Portal Informacyjny
                            </label>

                            <label class="radio-label">
                                <input type="radio" name="optradio">
                                <div class="checkmark"></div>
                                Tylko AdActa
                            </label>

                            <label class="radio-label">
                                <input type="radio" name="optradio">
                                <div class="checkmark"></div>
                                Archiwum
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel__footer">
                <div class="row">
                    <div class="col-12 d-flex justify-content-between">
                        <div class="btn btn-square btn-square--small d-flex justify-content-center align-items-center">
                            <div class="btn-block">
                                <div class="content"></div>
                            </div>
                        </div>
                        <div class="btn btn-square btn-square--small d-flex justify-content-center align-items-center">
                            <div class="btn-block">
                                <div class="content"></div>
                            </div>
                        </div>
                        <div class="btn btn-square btn-square--small d-flex justify-content-center align-items-center">
                            <div class="btn-block">
                                <div class="content"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="btn btn-square btn-square--large d-flex justify-content-center align-items-center">
                            <div class="btn-block">
                                <div class="content d-flex justify-content-center align-items-center">
                                    <div class="go-back-block">
                                        <p>powrót</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


