<div class="row">
    <div class="col-12 section-name__block">
        <div class="float-left h-100 w-100 d-flex justify-content-between justify-content-md-start align-items-center">
            <div class="logo-img__block">
                <div class="logo-img__block--square">
                    <img src="./img/adacta-favicon.svg" alt="Wirtualne Akta" class="img-fluid logo-img">
                </div>
            </div>
            <div class="logo-text__block d-none d-md-block">
                <p>panel główny</p>
            </div>
            <div class="logo-nav__block d-block d-md-none float-right h-100 d-flex flex-grow-1 align-items-center">
                <nav class="navigation float-right">
                    <ul class="nav navbar-nav flex-row justify-content-end">
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link navbar-nav__item__link--search d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--search"></div>
                            </a>
                        </li>
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link navbar-nav__item__link--messages d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--messages"></div>
                            </a>
                        </li>
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link navbar-nav__item__link--settings d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--settings"></div>
                            </a>
                        </li>
                        <li class="navbar-nav__item d-flex justify-content-center align-items-center">
                            <a class="navbar-nav__item__link navbar-nav__item__link--logout d-flex justify-content-center align-items-center" href="#">
                                <div class="navbar-nav__item__link__icon navbar-nav__item__link__icon--logout"></div>
                            </a>
                        </li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>
<div class="row flex-nowrap">
    <div class="nearest-dates-container active">
      <div class="swiper flex-column justify-content-center align-items-center">
        <div class="circle"></div>
        <div class="circle"></div>
        <div class="circle"></div>
      </div>
        <div class="my-3 shadow" style="background-color: #fff;" id="nearest-dates">
            <div class="row">
                <div class="col-12">
                    <div class="panel title-height title-block p-3">
                        <div class="panel__title margin-bottom-24 d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/ico-2.svg" alt="" height="35">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Najbliższe terminy</p>
                            </div>
                            <div class="panel__title--line-purple"></div>
                        </div>
                        <p>09 października 2018</p>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel content-height__main content-scroll px-3 mt-3">
                        <div class="panel__task task-done d-flex">
                            <div class="panel__task__colored-block d-flex justify-content-center align-items-center">
                                <div class="task-icon d-flex align-items-center">
                                    <img src="./img/task.svg" alt="ikona pobierania" height="15">
                                </div>
                            </div>
                            <div class="d-flex flex-column flex-md-row flex-grow-1 justify-content-center justify-content-md-between align-items-start align-items-md-center">
                                <div class="panel__task__text-block d-flex flex-column justify-content-center">
                                    <p class="title-text"><span>Negocjacje </span>Figat vs Kowalski i inni (7)</p>
                                    <p class="address-text">ul. Ks. Bogusława X 19/3</p>
                                    <p class="additional-info">Pierwsze spotkanie</p>
                                </div>
                                <div class="panel__task__time-block d-flex align-items-center">
                                    <p>2h30min</p>
                                </div>
                            </div>
                            <div class="panel__task__checkmark-block d-flex justify-content-center align-items-end align-items-md-center">
                                <div class="checkmark checked"></div>
                            </div>

                        </div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <p class="mt-4">10 października 2018</p>
                        <div class="panel--line-grey"></div>
                        <div class="panel__task d-flex">
                            <div class="panel__task__colored-block d-flex justify-content-center align-items-center">
                                <div class="task-icon d-flex align-items-center">
                                    <img src="./img/task.svg" alt="ikona pobierania" height="15">
                                </div>
                            </div>
                            <div class="d-flex flex-column flex-md-row flex-grow-1 justify-content-center justify-content-md-between align-items-start align-items-md-center">
                                <div class="panel__task__text-block d-flex flex-column justify-content-center">
                                    <p class="title-text"><span>Negocjacje </span>Figat vs Kowalski i inni (7)</p>
                                    <p class="address-text">ul. Ks. Bogusława X 19/3</p>
                                    <p class="additional-info">Pierwsze spotkanie</p>
                                </div>
                                <div class="panel__task__time-block d-flex align-items-center">
                                    <p>2h30min</p>
                                </div>
                            </div>
                            <div class="panel__task__checkmark-block d-flex justify-content-center align-items-end align-items-md-center">
                                <div class="checkmark"></div>
                            </div>
                        </div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="py-3 d-block d-xl-none panel-changer flip">
        <div class="h-100 shadow d-flex justify-content-center align-items-center" style="background-color: #fff;">
            <div class="change-panel">

            </div>
        </div>
    </div> -->
    <div class="recently-added-documents-container">
      <div class="swiper flex-column justify-content-center align-items-center">
        <div class="circle"></div>
        <div class="circle"></div>
        <div class="circle"></div>
      </div>
        <div class="my-3 shadow" style="background-color: #fff;" id="recently-added-documents">
            <div class="row">
                <div class="col-12">
                    <div class="panel title-height title-block px-3 mt-3">
                        <div class="panel__title margin-bottom-24 d-flex align-items-center">
                            <div class="panel__title__icon">
                                <img src="./img/ico-1.svg" alt="" height="35">
                            </div>
                            <div class="panel__title__block">
                                <p class="panel__title__block title-text">Ostatnio dodane dokumenty</p>
                            </div>
                            <div class="panel__title--line-purple"></div>
                        </div>
                        <p>20 września 2018</p>
                        <div class="panel--line-grey"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="panel content-height__main content-scroll px-3">
                        <div class="panel__task panel__task--document-list">
                            <div class="panel__task__status-block d-flex justify-content-center align-items-center">
                                <div class="task-icon d-flex justify-content-center align-items-center">
                                    <img src="./img/download-red-ico.svg" alt="ikona pobierania" height="15">
                                    <p>20-09-2018</p>
                                </div>
                                <div class="panel__task__text-block panel__task__text-block--top h-100 d-flex flex-grow-1 d-md-none flex-column justify-content-end justify-content-md-center">
                                    <p class="title-text text-right mr-1">Figat vs Kowalski i inni (7)</p>
                                </div>
                            </div>
                            <div class="panel__task__document-source"></div>
                            <div class="panel__task__text-block panel__task__text-block--bottom d-flex flex-column justify-content-center">
                                <p class="title-text d-none d-md-block">Figat vs Kowalski i inni (7)</p>
                                <p class="address-text">Wyznaczenie terminu posiedzenia</p>
                                <p class="additional-info"><span>Nadawca: </span>Sąd <span>Sygnatura: </span>III C 267/17</p>
                            </div>
                            <div class="panel__task__fileinfo-block d-flex justify-content-end justify-content-md-center align-items-center">
                                <img src="./img/from-system-ico.svg" alt="ikona pobierania" height="20">
                                <img src="./img/date-related-ico.svg" alt="ikona pobierania" height="20">
                            </div>
                        </div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                        <div class="panel__task"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
